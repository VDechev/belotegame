# BeloteGame

Belote game implemented by Veselin Dechev

About the game:
Belote is a 32-card, trick-taking, Ace-Ten game played primarily in France and certain European countries, 
namely Armenia, Bulgaria, Croatia, Cyprus, Greece, Moldova, Republic of Macedonia and also in Saudi Arabia. 
It is one of the most popular card games in those countries, and the national card game of France, both casually and in gambling. 
It was invented around 1920 in France, and is a close relative of both Klaberjass (also known as bela) and Klaverjas.
Closely related games are played throughout the world. Definitive rules of the game were first published in 1921.