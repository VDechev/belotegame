package com.vs.belote.game.players;

import org.junit.Assert;
import org.junit.Test;

public class VirtualPlayerTest {

    @Test
    public void askPlayerForAMove_returnTrue_whenNumberIsNotLessThanMinAndNotBiggerThanMax() {
        VirtualPlayer virtualPlayer = new VirtualPlayer("name");
        int min = 0;
        int max = 8;

        int number = virtualPlayer.askPlayerForAMove("", 0, 1);
        boolean result = number < max && number > min;

        Assert.assertTrue(result);
    }

    @Test
    public void equals_returnTrue_WhenPlayersAreWithSameName() {
        VirtualPlayer firstPlayer = new VirtualPlayer("name");
        VirtualPlayer secondPlayer = new VirtualPlayer("name");

        boolean result = firstPlayer.equals(secondPlayer) && secondPlayer.equals(firstPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void equals_returnFalse_WhenPlayersAreWithDifferentName() {
        VirtualPlayer firstPlayer = new VirtualPlayer("name");
        VirtualPlayer secondPlayer = new VirtualPlayer("differentName");

        boolean result = firstPlayer.equals(secondPlayer) && secondPlayer.equals(firstPlayer);

        Assert.assertFalse(result);
    }

    @Test
    public void hashCode_returnTrue_WhenPlayersAreWithSameName() {
        VirtualPlayer firstPlayer = new VirtualPlayer("name");
        VirtualPlayer secondPlayer = new VirtualPlayer("name");

        int firstPlayerHash = firstPlayer.hashCode();
        int secondPlayerHash = secondPlayer.hashCode();

        Assert.assertEquals(firstPlayerHash, secondPlayerHash);
    }
}