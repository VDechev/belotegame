package com.vs.belote.game.players;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;
import com.vs.belote.game.deck.Suit;
import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.io.CommandLineIO;
import com.vs.belote.game.io.InputOutput;
import com.vs.belote.game.rules.GameRules;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.OngoingStubbing;

import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HumanPlayerTest {
    private InputOutput io;
    GameRules gameRules;

    @Before
    public void mockIOAndGameRules() {
        io = mock(CommandLineIO.class);
        gameRules = mock(GameRules.class);
    }

    @Test
    public void equals_returnTrue_whenPlayersAreWithSameName() {
        HumanPlayer firstPlayer = new HumanPlayer("player", new CommandLineIO());
        HumanPlayer secondPlayer = new HumanPlayer("player", new CommandLineIO());

        boolean result = firstPlayer.equals(secondPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void equals_returnFalse_whenPlayersAreWithDifferentName() {
        HumanPlayer firstPlayer = new HumanPlayer("player", new CommandLineIO());
        HumanPlayer secondPlayer = new HumanPlayer("player2", new CommandLineIO());

        boolean result = firstPlayer.equals(secondPlayer);

        Assert.assertFalse(result);
    }

    @Test
    public void equals_returnFalse_whenNoSecondPlayer() {
        HumanPlayer firstPlayer = new HumanPlayer("player", new CommandLineIO());

        boolean result = firstPlayer.equals(null);

        Assert.assertFalse(result);
    }

    @Test
    public void makeABid_returnTrue_whenValidInputIsGiven() {
        setCalls(new String[]{"4"});
        HumanPlayer humanPlayer = new HumanPlayer("name", io);

        Bid result = humanPlayer.makeABid(Bid.SPADES, false);

        Assert.assertEquals(Bid.CLUBS, result);

    }

    @Test
    public void makeABid_returnTrue_whenInvalidAndThenValidInputIsGiven() {
        setCalls(new String[]{"1", "4"});
        HumanPlayer humanPlayer = new HumanPlayer("name", io);

        Bid result = humanPlayer.makeABid(Bid.SPADES, false);

        Assert.assertEquals(Bid.CLUBS, result);

    }

    @Test
    public void askPlayerForAMove_returnValidInput_whenInputIsInRange() {
        setCalls(new String[]{"5"});
        HumanPlayer humanPlayer = new HumanPlayer("name", io);

        int result = humanPlayer.askPlayerForAMove("", 2, 6);

        Assert.assertEquals(5, result);
    }

    @Test
    public void askPlayerForAMove_returnValidInput_whenInvalidAndThenInputInRangeIsGivevn() {
        setCalls(new String[]{"1", "5"});
        HumanPlayer humanPlayer = new HumanPlayer("name", io);

        int result = humanPlayer.askPlayerForAMove("", 2, 6);

        Assert.assertEquals(5, result);
    }


    @Test
    public void playACard_returnCard_whenInputIsValid(){
        setCalls(new String[]{"0"});
        Card cardForPlaying = new Card(Rank.DIAMONDS, Suit.KING,4,4);
        HumanPlayer humanPlayer = new HumanPlayer("",io);
        humanPlayer.addCardsToHand(new ArrayList<>(Collections.singletonList(cardForPlaying)));
        when(gameRules.canIPlayThisCard(null,cardForPlaying,
                humanPlayer.getHand(),null,null)).thenReturn(true);

        Card result = humanPlayer.playCard(null,gameRules,null,null);

        Assert.assertEquals(cardForPlaying,result);
    }

    private void setCalls(String[] calls) {
        OngoingStubbing<String> ongoingStubbing = when(io.readLine());
        for (String string : calls) {
            ongoingStubbing = ongoingStubbing.thenReturn(string);
        }
    }
}