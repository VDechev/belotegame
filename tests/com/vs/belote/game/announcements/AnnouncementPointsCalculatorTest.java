package com.vs.belote.game.announcements;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;
import com.vs.belote.game.deck.Suit;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AnnouncementPointsCalculatorTest {

    @Test
    public void announcementPointsCalculator_returnOneHundred_whenListWithAceSquareIsGiven() {
        int expected = 100;
        List<Announcement> announcementsForCalculating = new ArrayList<>();
        announcementsForCalculating.add(new Announcement(Announcements.SQUARE, 100,
                new Card(Rank.DIAMONDS, Suit.ACE, 11, 11)));

        int result = AnnouncementPointsCalculator.announcementPointsCalculator(announcementsForCalculating);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void announcementPointsCalculator_returnOneHundred_whenListWithFiveTiercesIsGiven() {
        int expected = 100;
        List<Announcement> announcementsForCalculating = new ArrayList<>();
        Announcement tierceAnnouncement = new Announcement(Announcements.SQUARE, 20,
                new Card(Rank.DIAMONDS, Suit.ACE, 11, 11));
        announcementsForCalculating.add(tierceAnnouncement);
        announcementsForCalculating.add(tierceAnnouncement);
        announcementsForCalculating.add(tierceAnnouncement);
        announcementsForCalculating.add(tierceAnnouncement);
        announcementsForCalculating.add(tierceAnnouncement);

        int result = AnnouncementPointsCalculator.announcementPointsCalculator(announcementsForCalculating);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void announcementPointsCalculator_returnZero_whenEmptyListIsGiven() {
        int expected = 0;
        List<Announcement> announcementsForCalculating = new ArrayList<>();

        int result = AnnouncementPointsCalculator.announcementPointsCalculator(announcementsForCalculating);

        Assert.assertEquals(expected, result);
    }

}