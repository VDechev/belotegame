package com.vs.belote.game.announcements;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;
import com.vs.belote.game.deck.Suit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AnnouncementCheckerTest {

    private Card sevenSpades;
    private Card eightSpades;
    private Card nineSpades;
    private Card tenSpades;
    private Card jackSpades;
    private Card jackHearts;
    private Card queenHearts;
    private Card kingHearts;
    private Card jackDiamonds;
    private Card jackClubs;

    @Before
    public void initializeCards() {
        sevenSpades = new Card(Rank.SPADES, Suit.SEVEN, 0, 0);
        eightSpades = new Card(Rank.SPADES, Suit.EIGHT, 0, 0);
        nineSpades = new Card(Rank.SPADES, Suit.NINE, 14, 0);
        tenSpades = new Card(Rank.SPADES, Suit.TEN, 10, 10);
        jackSpades = new Card(Rank.SPADES, Suit.JACK, 20, 2);
        jackHearts = new Card(Rank.HEARTS, Suit.JACK, 20, 2);
        queenHearts = new Card(Rank.HEARTS, Suit.QUEEN, 3, 3);
        kingHearts = new Card(Rank.HEARTS, Suit.KING, 4, 4);
        jackDiamonds = new Card(Rank.DIAMONDS, Suit.JACK, 20, 2);
        jackClubs = new Card(Rank.CLUBS, Suit.JACK, 20, 2);
    }

    @Test
    public void getPlayerAnnouncement_returnTwoTierce_whenHandWithTwoTierce() {
        List<Card> playersHand = createHandWithTwoTierce();
        Announcement firstTierce = new Announcement(Announcements.TIERCE, 20, nineSpades);
        Announcement secondTierce = new Announcement(Announcements.TIERCE, 20, kingHearts);

        List<Announcement> result = AnnouncementChecker.getPlayerAnnouncements(playersHand);

        assertEquals(firstTierce, result.get(0));
        assertEquals(secondTierce, result.get(1));
    }

    @Test
    public void getPlayerAnnouncement_returnTierce_whenHandWithTierce() {
        List<Card> playersHand = createHandWithTierce();
        Announcement tierce = new Announcement(Announcements.TIERCE, 20, kingHearts);

        List<Announcement> result = AnnouncementChecker.getPlayerAnnouncements(playersHand);

        assertEquals(tierce, result.get(0));
    }

    @Test
    public void getPlayerAnnouncement_returnNull_whenHandWithNoAnnouncements() {
        List<Card> playersHand = createHandWithNoAnnouncements();

        List<Announcement> result = AnnouncementChecker.getPlayerAnnouncements(playersHand);

        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void getPlayerAnnouncement_returnSquare_whenHandWithSquare() {
        List<Card> playersHand = createHandWithSquare();
        Announcement square = new Announcement(Announcements.SQUARE, 200, jackSpades);

        List<Announcement> result = AnnouncementChecker.getPlayerAnnouncements(playersHand);

        assertEquals(square, result.get(0));
    }

    @Test
    public void getPlayerAnnouncement_returnQuarte_whenHandWithQuarte() {
        List<Card> playersHand = createHandWithQuarte();
        Announcement quarte = new Announcement(Announcements.QUARTE, 50, tenSpades);

        List<Announcement> result = AnnouncementChecker.getPlayerAnnouncements(playersHand);

        assertEquals(quarte, result.get(0));
    }

    @Test
    public void getPlayerAnnouncement_returnQuinte_whenHandWithQuinte() {
        List<Card> playersHand = createHandWithQuinte();
        Announcement quinte = new Announcement(Announcements.QUINTE, 100, jackSpades);

        List<Announcement> result = AnnouncementChecker.getPlayerAnnouncements(playersHand);

        assertEquals(quinte, result.get(0));
    }

    private List<Card> createHandWithTwoTierce() {
        return new ArrayList<Card>(
                Arrays.asList(sevenSpades, eightSpades, nineSpades, jackDiamonds, jackClubs, jackHearts, queenHearts, kingHearts));
    }

    private List<Card> createHandWithTierce() {
        return new ArrayList<Card>(
                Arrays.asList(sevenSpades, tenSpades, nineSpades, jackDiamonds, jackClubs, jackHearts, queenHearts, kingHearts));
    }

    private List<Card> createHandWithNoAnnouncements() {
        return new ArrayList<Card>(
                Arrays.asList(sevenSpades, tenSpades, nineSpades, jackDiamonds, jackClubs, jackHearts, kingHearts));
    }

    private List<Card> createHandWithSquare() {
        return new ArrayList<Card>(
                Arrays.asList(sevenSpades, jackSpades, nineSpades, jackDiamonds, jackClubs, jackHearts, kingHearts));
    }

    private List<Card> createHandWithQuarte() {
        return new ArrayList<Card>(
                Arrays.asList(sevenSpades, eightSpades, nineSpades, tenSpades, jackClubs, jackHearts, kingHearts));
    }

    private List<Card> createHandWithQuinte() {
        return new ArrayList<Card>(
                Arrays.asList(sevenSpades, eightSpades, nineSpades, tenSpades, jackSpades, jackHearts, kingHearts));
    }
}
