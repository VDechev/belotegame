package com.vs.belote.game.announcements;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;
import com.vs.belote.game.deck.Suit;
import org.junit.Assert;
import org.junit.Test;

public class AnnouncementTest {

    @Test
    public void compareTo_returnPositiveNumber_WhenFirstAnnouncementIsBigger() {
        Announcement announcement = new Announcement(Announcements.SQUARE, 150,
                new Card(Rank.DIAMONDS, Suit.KING, 4, 4));

        int result = announcement.compareTo(new Announcement(
                Announcements.TIERCE, 20,
                new Card(Rank.DIAMONDS, Suit.KING, 4, 4)));

        Assert.assertTrue(result > 0);
    }

    @Test
    public void compareTo_returnNegativeNumber_WhenFirstAnnouncementIsBigger() {
        Announcement announcement = new Announcement(Announcements.TIERCE, 20,
                new Card(Rank.DIAMONDS, Suit.KING, 4, 4));

        int result = announcement.compareTo(new Announcement(
                Announcements.SQUARE, 150,
                new Card(Rank.DIAMONDS, Suit.KING, 4, 4)));

        Assert.assertTrue(result < 0);
    }

    @Test
    public void compareTo_returnZero_WhenFirstAnnouncementIsBigger() {
        Announcement announcement = new Announcement(Announcements.TIERCE, 20,
                new Card(Rank.DIAMONDS, Suit.KING, 4, 4));

        int result = announcement.compareTo(new Announcement(
                Announcements.TIERCE, 20,
                new Card(Rank.DIAMONDS, Suit.KING, 4, 4)));

        Assert.assertTrue(result == 0);
    }

}