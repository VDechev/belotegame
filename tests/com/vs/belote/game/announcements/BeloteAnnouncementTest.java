package com.vs.belote.game.announcements;

import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;
import com.vs.belote.game.deck.Suit;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BeloteAnnouncementTest {

    @Test
    public void isBelote_returnTrue_whenBeloteCardsOnAllTrumpsIsGiven() {
        List<Card> playersHand = new ArrayList<>();
        Card kingCard = new Card(Rank.DIAMONDS, Suit.KING, 4, 4);
        Card queenCard = new Card(Rank.DIAMONDS, Suit.QUEEN, 3, 3);
        playersHand.add(kingCard);
        playersHand.add(queenCard);

        boolean result = BeloteAnnouncement.isBelote(kingCard, playersHand, Bid.ALLTRUMPS);

        Assert.assertTrue(result);
    }


    @Test
    public void isBelote_returnFalse_whenBeloteCardsOnNoTrumpsIsGiven() {
        List<Card> playersHand = new ArrayList<>();
        Card kingCard = new Card(Rank.DIAMONDS, Suit.KING, 4, 4);
        Card queenCard = new Card(Rank.DIAMONDS, Suit.QUEEN, 3, 3);
        playersHand.add(kingCard);
        playersHand.add(queenCard);

        boolean result = BeloteAnnouncement.isBelote(kingCard, playersHand, Bid.NOTRUMPS);

        Assert.assertFalse(result);
    }

    @Test
    public void isBelote_returnFalse_whenBeloteCardsOnDifferentRuleIsGiven() {
        List<Card> playersHand = new ArrayList<>();
        Card kingCard = new Card(Rank.DIAMONDS, Suit.KING, 4, 4);
        Card queenCard = new Card(Rank.DIAMONDS, Suit.QUEEN, 3, 3);
        playersHand.add(kingCard);
        playersHand.add(queenCard);

        boolean result = BeloteAnnouncement.isBelote(kingCard, playersHand, Bid.CLUBS);

        Assert.assertFalse(result);
    }

    @Test
    public void isBelote_returnFalse_whenCardsWithNoQueenAreGiven() {
        List<Card> playersHand = new ArrayList<>();
        Card kingCard = new Card(Rank.DIAMONDS, Suit.KING, 4, 4);
        Card queenCard = new Card(Rank.DIAMONDS, Suit.SEVEN, 3, 3);
        playersHand.add(kingCard);
        playersHand.add(queenCard);

        boolean result = BeloteAnnouncement.isBelote(kingCard, playersHand, Bid.CLUBS);

        Assert.assertFalse(result);
    }
}