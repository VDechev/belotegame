package com.vs.belote.game.announcements;

import com.vs.belote.game.auxiliaryclasses.Either;
import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;
import com.vs.belote.game.deck.Suit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AnnouncementComparatorTest {
    private Announcement tierceAnnouncementForTests;
    private Announcement secondTierceAnnouncementForTests;
    private Announcement jackSquareAnnouncementForTests;
    private Announcement nineSquareAnnouncementForTests;
    private Announcement quinteAnnouncementForTests;
    private Announcement quatreAnnouncementForTests;

    @Before
    public void initializeAnnouncements() {
        Card jackCard = new Card(Rank.DIAMONDS, Suit.JACK, 20, 2);
        Card nineCard = new Card(Rank.SPADES, Suit.NINE, 14, 0);

        tierceAnnouncementForTests = new Announcement(Announcements.TIERCE, 20, nineCard);
        secondTierceAnnouncementForTests = new Announcement(Announcements.TIERCE, 20, jackCard);
        jackSquareAnnouncementForTests = new Announcement(Announcements.SQUARE, 200, jackCard);
        nineSquareAnnouncementForTests = new Announcement(Announcements.SQUARE, 100, nineCard);
        quinteAnnouncementForTests = new Announcement(Announcements.QUINTE, 100, nineCard);
        quatreAnnouncementForTests = new Announcement(Announcements.QUARTE, 50, jackCard);
    }

    @Test
    public void announceComparator_returnFirstTeamAnnouncements_WhenFirstTeamAnnounceAreBigger() {
        List<Announcement> firstTeamAnnouncements = new ArrayList<>();
        List<Announcement> secondTeamAnnouncements = new ArrayList<>();
        firstTeamAnnouncements.add(quinteAnnouncementForTests);
        secondTeamAnnouncements.add(quatreAnnouncementForTests);

        Either result = AnnouncementComparator.announceComparator(
                firstTeamAnnouncements, secondTeamAnnouncements);

        Assert.assertTrue(result.isLeft());
    }

    @Test
    public void announceComparator_returnSecondTeamAnnouncements_WhenSecondTeamAnnounceAreBigger() {
        List<Announcement> firstTeamAnnouncements = new ArrayList<>();
        List<Announcement> secondTeamAnnouncements = new ArrayList<>();
        secondTeamAnnouncements.add(quinteAnnouncementForTests);
        firstTeamAnnouncements.add(quatreAnnouncementForTests);

        Either result = AnnouncementComparator.announceComparator(
                firstTeamAnnouncements, secondTeamAnnouncements);

        Assert.assertTrue(result.isRight());
    }

    @Test
    public void announceComparator_returnFirstTeamAnnouncements_WhenSecondTeamHaveNoAnnouncements() {
        List<Announcement> firstTeamAnnouncements = new ArrayList<>();
        List<Announcement> secondTeamAnnouncements = new ArrayList<>();
        firstTeamAnnouncements.add(quatreAnnouncementForTests);

        Either result = AnnouncementComparator.announceComparator(
                firstTeamAnnouncements, secondTeamAnnouncements);

        Assert.assertTrue(result.isLeft());
    }

    @Test
    public void announceComparator_returnSecondTeamAnnouncements_WhenFirstTeamHaveNoAnnouncements() {
        List<Announcement> firstTeamAnnouncements = new ArrayList<>();
        List<Announcement> secondTeamAnnouncements = new ArrayList<>();
        secondTeamAnnouncements.add(quatreAnnouncementForTests);

        Either result = AnnouncementComparator.announceComparator(
                firstTeamAnnouncements, secondTeamAnnouncements);

        Assert.assertTrue(result.isRight());
    }

    @Test
    public void announceComparator_returnNull_WhenTeamsHaveSameAnnouncements() {
        List<Announcement> firstTeamAnnouncements = new ArrayList<>();
        List<Announcement> secondTeamAnnouncements = new ArrayList<>();
        secondTeamAnnouncements.add(tierceAnnouncementForTests);
        firstTeamAnnouncements.add(tierceAnnouncementForTests);

        Either result = AnnouncementComparator.announceComparator(
                firstTeamAnnouncements, secondTeamAnnouncements);

        Assert.assertNull(result);
    }

    @Test
    public void announceComparator_returnFirstTeamAnnouncements_WhenFirstTeamHaveBiggerTierce() {
        List<Announcement> firstTeamAnnouncements = new ArrayList<>();
        List<Announcement> secondTeamAnnouncements = new ArrayList<>();
        secondTeamAnnouncements.add(tierceAnnouncementForTests);
        firstTeamAnnouncements.add(secondTierceAnnouncementForTests);

        Either result = AnnouncementComparator.announceComparator(
                firstTeamAnnouncements, secondTeamAnnouncements);

        Assert.assertTrue(result.isLeft());
    }
}