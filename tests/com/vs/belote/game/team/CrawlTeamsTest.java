package com.vs.belote.game.team;

import com.vs.belote.game.players.Player;
import com.vs.belote.game.players.VirtualPlayer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CrawlTeamsTest {
    private Player firstPlayer;
    private Player secondPlayer;
    private Player thirdPlayer;
    private Player fourthPlayer;
    private CrawlTeams crawlTeams;
    private Team firstTeam;
    private Team secondTeam;

    @Before
    public void initializeTeams() {
        firstPlayer = new VirtualPlayer("");
        secondPlayer = new VirtualPlayer("");
        thirdPlayer = new VirtualPlayer("");
        fourthPlayer = new VirtualPlayer("");
        firstTeam = new Team();
        firstTeam.addPlayer(firstPlayer, 1);
        firstTeam.addPlayer(thirdPlayer);
        secondTeam = new Team();
        secondTeam.addPlayer(secondPlayer, 1);
        secondTeam.addPlayer(fourthPlayer);
    }

    @Test
    public void getDealer_returnDealer_whenCalled() {
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 4);

        Player dealer = crawlTeams.getDealer();

        Assert.assertEquals(fourthPlayer, dealer);
    }

    @Test
    public void getNextPlayerToDeal_returnNextDealer_whenCalled() {
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 1);

        Player dealer = crawlTeams.getNextPlayerToDeal();

        Assert.assertEquals(secondPlayer, dealer);
    }

    @Test
    public void getNextPlayer_returnNextPlayer_whenCalledWithDealerAsArgument() {
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 1);

        Player result = crawlTeams.getDealer();
        result = crawlTeams.getNextPlayer(result);

        Assert.assertEquals(secondPlayer, result);
    }

    @Test
    public void getNextPlayer_returnFirstPlayer_whenCalledWithLastPlayer() {
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 1);

        Player result = crawlTeams.getNextPlayer(fourthPlayer);

        Assert.assertEquals(firstPlayer, result);
    }

    @Test
    public void getNextPlayerFromPlayer_returnFirstPlayer_whenCalledWithLastPlayer() {
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 1);

        Player result = crawlTeams.getNextPlayer(fourthPlayer);

        Assert.assertEquals(firstPlayer, result);
    }

    @Test
    public void getPlayersTeam_returnFirstTeam_whenCalledWithPlayerFromFirstTeam() {
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 1);

        Team result = crawlTeams.getPlayersTeam(thirdPlayer);

        Assert.assertEquals(firstTeam, result);
    }

    @Test
    public void getPlayersTeam_returnSecondTeam_whenCalledWithPlayerFromSecondTeam() {
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 1);

        Team result = crawlTeams.getPlayersTeam(fourthPlayer);

        Assert.assertEquals(secondTeam, result);
    }

    @Test
    public void arePlayersFromTheSameTeam_returnTrue_whenCalledWithPlayersFromTheSameTeam() {
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 1);

        boolean result = crawlTeams.arePlayersFromSameTeam(firstPlayer, thirdPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void arePlayersFromTheSameTeam_returnTrue_whenCalledWithSecondAndFourthPlayer() {
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 1);

        boolean result = crawlTeams.arePlayersFromSameTeam(secondPlayer, fourthPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void arePlayersFromTheSameTeam_returnFalse_whenCalledWithPlayersFromDifferentTeams() {
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 1);

        boolean result = crawlTeams.arePlayersFromSameTeam(secondPlayer, thirdPlayer);

        Assert.assertTrue(result);
    }
}