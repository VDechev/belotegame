package com.vs.belote.game.team;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;
import com.vs.belote.game.deck.Suit;
import com.vs.belote.game.io.CommandLineIO;
import com.vs.belote.game.io.InputOutput;
import com.vs.belote.game.players.HumanPlayer;
import com.vs.belote.game.players.Player;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.OngoingStubbing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FillTeamsTest {
    private FillTeams fillTeams;
    private InputOutput io;
    private List<Card> blackAndRedCards;

    @Before
    public void mockIOAndGetBlackAndRedCards() {
        Card firstRedCard = new Card(Rank.DIAMONDS, Suit.KING, 4, 4);
        Card secondRedCard = new Card(Rank.HEARTS, Suit.KING, 4, 4);
        Card firstBlackCard = new Card(Rank.SPADES, Suit.KING, 4, 4);
        Card secondBlackCard = new Card(Rank.CLUBS, Suit.KING, 4, 4);
        blackAndRedCards = new ArrayList<>(Arrays.asList(
                firstRedCard, secondRedCard, firstBlackCard, secondBlackCard));
        io = mock(CommandLineIO.class);
    }

    @Test
    public void getTeams_createTwoTeams_whenSystemInIsRedirected() {
        setCalls(new String[]{"first", "1", "1", "1", "second", "1", "1", "third", "1", "1", "1",
                "fourth", "1", "1"});
        Player firstPlayer = new HumanPlayer("first", io);
        Player secondPlayer = new HumanPlayer("second", io);
        Player thirdPlayer = new HumanPlayer("third", io);
        Player fourthPlayer = new HumanPlayer("fourth", io);
        Team teamWithRedCards = createTeam(firstPlayer, secondPlayer);
        Team teamWithBlackCards = createTeam(thirdPlayer, fourthPlayer);

        fillTeams = new FillTeams(io, blackAndRedCards);

        Assert.assertEquals(teamWithRedCards, fillTeams.getTeamWithRedCards());
        Assert.assertEquals(teamWithBlackCards, fillTeams.getTeamWithBlackCards());
    }

    private Team createTeam(Player firstPlayer, Player secondPlayer) {
        Team team = new Team();
        team.addPlayer(firstPlayer, 1);
        team.addPlayer(secondPlayer);
        return team;
    }

    private void setCalls(String[] calls) {
        OngoingStubbing<String> ongoingStubbing = when(io.readLine());
        for (String string : calls) {
            ongoingStubbing = ongoingStubbing.thenReturn(string);
        }
    }
}