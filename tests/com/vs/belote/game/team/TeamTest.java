package com.vs.belote.game.team;

import com.vs.belote.game.io.CommandLineIO;
import com.vs.belote.game.players.HumanPlayer;
import com.vs.belote.game.players.Player;
import org.junit.Assert;
import org.junit.Test;

public class TeamTest {

    @Test
    public void isTeamEmpty_returnTrue_whenTeamIsEmpty() {
        Team teamForTest = new Team();

        Assert.assertTrue(teamForTest.isTeamEmpty());
    }

    @Test
    public void isTeamEmpty_returnFalse_whenTeamIsNotEmpty() {
        Team teamForTest = new Team();

        teamForTest.addPlayer(new HumanPlayer("", new CommandLineIO()));

        Assert.assertFalse(teamForTest.isTeamEmpty());
    }

    @Test
    public void addPlayer_returnTrue_whenAddNewPlayer() {
        Team teamForTest = new Team();
        Player playerForTest = new HumanPlayer("player", new CommandLineIO());
        teamForTest.addPlayer(playerForTest, 1);

        Assert.assertTrue(teamForTest.isPlayerFromThisTeam(playerForTest));
    }

    @Test
    public void addPlayer_returnFalse_whenAddPlayerWhenPlayerAlreadyExists() {
        Team teamForTest = new Team();
        Player playerForTest = new HumanPlayer("", new CommandLineIO());
        Player newPlayer = new HumanPlayer("newPlayer", new CommandLineIO());

        teamForTest.addPlayer(playerForTest, 1);
        teamForTest.addPlayer(newPlayer, 1);

        Assert.assertFalse(teamForTest.isPlayerFromThisTeam(newPlayer));
    }

    @Test
    public void addPlayer_returnTrue_whenAddOtherPlayer() {
        Team teamForTest = new Team();
        Player playerForTest = new HumanPlayer("", new CommandLineIO());
        Player secondPlayerForTest = new HumanPlayer("secondPlayer", new CommandLineIO());

        teamForTest.addPlayer(playerForTest, 1);
        teamForTest.addPlayer(secondPlayerForTest);

        Assert.assertTrue(teamForTest.isPlayerFromThisTeam(secondPlayerForTest));
    }

    @Test
    public void addPlayer_returnFalse_whenAddOtherPlayerWhenOtherPlayerAlreadyExists() {
        Team teamForTest = new Team();
        Player playerForTest = new HumanPlayer("", new CommandLineIO());
        Player secondPlayerForTest = new HumanPlayer("secondPlayer", new CommandLineIO());
        Player newPlayer = new HumanPlayer("newPlayer", new CommandLineIO());


        teamForTest.addPlayer(playerForTest, 1);
        teamForTest.addPlayer(secondPlayerForTest);
        teamForTest.addPlayer(newPlayer);

        Assert.assertFalse(teamForTest.isPlayerFromThisTeam(newPlayer));
    }

    @Test
    public void isPlayerFromThisTeam_returnTrue_whenPlayerIsFromThisTeam() {
        Team teamForTest = new Team();
        Player playerForTest = new HumanPlayer("", new CommandLineIO());

        teamForTest.addPlayer(playerForTest, 1);

        Assert.assertTrue(teamForTest.isPlayerFromThisTeam(playerForTest));
    }

    @Test
    public void isPlayerFromThisTeam_returnFalse_whenPlayerIsNotFromThisTeam() {
        Team teamForTest = new Team();
        Player playerForTest = new HumanPlayer("", new CommandLineIO());
        Player playerNotFromTheTeam = new HumanPlayer("notFromThisTeam", new CommandLineIO());

        teamForTest.addPlayer(playerForTest, 1);

        Assert.assertFalse(teamForTest.isPlayerFromThisTeam(playerNotFromTheTeam));
    }

    @Test
    public void equals_returnFalse_whenPlayerIsNotEqual() {
        Team teamForTest = new Team();
        Player playerForTest = new HumanPlayer("", new CommandLineIO());
        Player playerNotFromTheTeam = new HumanPlayer("notFromThisTeam", new CommandLineIO());

        Assert.assertNotEquals(playerForTest, playerNotFromTheTeam);
    }

    @Test
    public void equals_returnTrue_whenPlayersAreEqual() {
        Team teamForTest = new Team();
        Player playerForTest = new HumanPlayer("same", new CommandLineIO());
        Player playerNotFromTheTeam = new HumanPlayer("same", new CommandLineIO());

        Assert.assertEquals(playerForTest, playerNotFromTheTeam);
    }
}