package com.vs.belote.game;

import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.players.VirtualPlayer;
import com.vs.belote.game.team.CrawlTeams;
import com.vs.belote.game.team.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class ActionValidatorTest {
    private Player firstPlayer;
    private Player secondPlayer;
    private Player thirdPlayer;
    private Player fourthPlayer;
    private CrawlTeams crawlTeams;

    @Before
    public void initializeTeams() {
        firstPlayer = new VirtualPlayer("");
        secondPlayer = new VirtualPlayer("");
        thirdPlayer = new VirtualPlayer("");
        fourthPlayer = new VirtualPlayer("");
        Team firstTeam = new Team();
        firstTeam.addPlayer(secondPlayer, 1);
        firstTeam.addPlayer(firstPlayer);
        Team secondTeam = new Team();
        secondTeam.addPlayer(fourthPlayer, 2);
        secondTeam.addPlayer(thirdPlayer);
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 1);
    }

    @Test
    public void canIMakeDouble_returnFalse_whenPlayersAreFromTheSameTeam() {
        Player firstPlayer = mock(Player.class);
        Player secondPlayer = mock(Player.class);
        CrawlTeams crawlTeams = mock(CrawlTeams.class);
        when(crawlTeams.arePlayersFromSameTeam(firstPlayer, secondPlayer)).thenReturn(true);

        boolean result = ActionValidator.arePlayersFromTheSameTeam(firstPlayer, secondPlayer, crawlTeams);

        Assert.assertTrue(result);
    }

    @Test
    public void canIMakeThisBid_returnFalse_whenDoubleWhilePlayerIsFirstBidder() {
        Bid bid = null;

        boolean result = ActionValidator.isPossibleBid(bid, 7, true);

        Assert.assertFalse(result);
    }

    @Test
    public void canIMakeThisBid_returnFalse_whenDoubleWhileHigherBidIsPass() {
        Bid bid = Bid.PASS;

        boolean result = ActionValidator.isPossibleBid(bid, 7, true);

        Assert.assertFalse(result);
    }

    @Test
    public void canIMakeThisBid_returnFalse_whenReDoubleWithoutDouble() {
        Bid bid = Bid.CLUBS;

        boolean result = ActionValidator.isPossibleBid(bid, 8, false);

        Assert.assertFalse(result);
    }

    @Test
    public void canIMakeThisBid_returnFalse_whenReDoubleOnTeammate() {
        Bid bid = Bid.DIAMONDS;
        bid.setDoublePoints(true);

        boolean result = ActionValidator.isPossibleBid(bid, 8, true);

        Assert.assertFalse(result);
    }

    @Test
    public void canIMakeThisBid_returnFalse_whenDoubleOnTeammate() {
        Bid bid = Bid.DIAMONDS;

        boolean result = ActionValidator.isPossibleBid(bid, 7, true);

        Assert.assertFalse(result);
    }

    @Test
    public void canIMakeThisBid_returnFalse_whenDoubleWhenAlreadyDouble() {
        Bid bid = Bid.DIAMONDS;
        bid.setDoublePoints(true);

        boolean result = ActionValidator.isPossibleBid(bid, 7, false);

        Assert.assertFalse(result);
    }

    @Test
    public void canIMakeThisBid_returnFalse_whenReDoubleWhenAlreadyReDouble() {
        Bid bid = Bid.DIAMONDS;
        bid.setDoublePoints(true);
        bid.setReDoublePoints(true);

        boolean result = ActionValidator.isPossibleBid(bid, 7, false);

        Assert.assertFalse(result);
    }

    @Test
    public void canIMakeThisBid_returnFalse_whenLowerBid() {
        Bid bid = Bid.DIAMONDS;

        boolean result = ActionValidator.isPossibleBid(bid, 1, false);

        Assert.assertFalse(result);
    }

    @Test
    public void canIMakeThisBid_returnFalse_whenSameBidAsHighest() {
        Bid bid = Bid.DIAMONDS;

        boolean result = ActionValidator.isPossibleBid(bid, 2, false);

        Assert.assertFalse(result);
    }

    @Test
    public void canIMakeThisBid_returnTrue_whenHigher() {
        Bid bid = Bid.DIAMONDS;

        boolean result = ActionValidator.isPossibleBid(bid, 4, false);

        Assert.assertTrue(result);
    }

    @Test
    public void canIMakeThisBid_returnTrue_whenDouble() {
        Bid bid = Bid.SPADES;

        boolean result = ActionValidator.isPossibleBid(bid, 7, false);

        Assert.assertTrue(result);
    }

    @Test
    public void canIMakeThisBid_returnTrue_whenReDouble() {
        Bid bid = Bid.DIAMONDS;
        bid.setDoublePoints(true);

        boolean result = ActionValidator.isPossibleBid(bid, 8, false);

        Assert.assertTrue(result);
    }
}