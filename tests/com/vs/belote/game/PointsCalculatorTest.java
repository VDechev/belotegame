package com.vs.belote.game;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;
import com.vs.belote.game.deck.Suit;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.players.VirtualPlayer;
import com.vs.belote.game.rules.AllTrumpsRules;
import com.vs.belote.game.team.CrawlTeams;
import com.vs.belote.game.team.Team;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PointsCalculatorTest {
    private Player firstPlayer;
    private Player secondPlayer;
    private Player thirdPlayer;
    private Player fourthPlayer;
    private Team firstTeam;
    private Team secondTeam;

    @Test
    public void calculateTeamsPoints_returnZero_whenFirstTeamIsValat() {
        List<Card> secondTeamPile = fillTeamPile();
        List<Card> firstTeamPile = new ArrayList<>();
        PointsCalculator pointsCalculator = new PointsCalculator();
        int expectedHandingPoints = 0;
        int expectedFirstTeamPoints = 0;
        int expectedSecondTeamPoints = 15;
        initializePlayersAndTeams();

        int result = pointsCalculator.calculateTeamsPoints(firstTeam, secondTeam, firstTeamPile, secondTeamPile, new AllTrumpsRules(), thirdPlayer, fourthPlayer, false, false, 0,
                new ArrayList<>(), new ArrayList<>(), 0, 0);

        Assert.assertEquals(expectedHandingPoints, result);
        Assert.assertEquals(expectedFirstTeamPoints, firstTeam.getTeamPoints());
        Assert.assertEquals(expectedSecondTeamPoints, secondTeam.getTeamPoints());
    }

    @Test
    public void calculateTeamsPoints_returnZero_whenFirstTeamIsValatWithDouble() {
        List<Card> secondTeamPile = fillTeamPile();
        List<Card> firstTeamPile = new ArrayList<>();
        PointsCalculator pointsCalculator = new PointsCalculator();
        int expectedHandingPoints = 0;
        int expectedFirstTeamPoints = 0;
        int expectedSecondTeamPoints = 15;
        initializePlayersAndTeams();

        int result = pointsCalculator.calculateTeamsPoints(firstTeam, secondTeam, firstTeamPile, secondTeamPile, new AllTrumpsRules(), thirdPlayer, fourthPlayer, true, false, 0,
                new ArrayList<>(), new ArrayList<>(), 0, 0);

        Assert.assertEquals(expectedHandingPoints, result);
        Assert.assertEquals(expectedFirstTeamPoints, firstTeam.getTeamPoints());
        Assert.assertEquals(expectedSecondTeamPoints, secondTeam.getTeamPoints());
    }

    @Test
    public void calculateTeamsPoints_returnZero_whenFirstTeamIsWithLessCardsAndDouble() {
        List<Card> secondTeamPile = fillTeamPile();
        List<Card> firstTeamPile = fillTeamPile();
        secondTeamPile.addAll(fillTeamPile());
        PointsCalculator pointsCalculator = new PointsCalculator();
        int expectedHandingPoints = 0;
        int expectedFirstTeamPoints = 0;
        int expectedSecondTeamPoints = 29;
        initializePlayersAndTeams();

        int result = pointsCalculator.calculateTeamsPoints(firstTeam, secondTeam, firstTeamPile, secondTeamPile, new AllTrumpsRules(), thirdPlayer, fourthPlayer, true, false, 0,
                new ArrayList<>(), new ArrayList<>(), 0, 0);

        Assert.assertEquals(expectedHandingPoints, result);
        Assert.assertEquals(expectedFirstTeamPoints, firstTeam.getTeamPoints());
        Assert.assertEquals(expectedSecondTeamPoints, secondTeam.getTeamPoints());
    }

    @Test
    public void calculateTeamsPoints_returnZero_whenFirstTeamIsScored() {
        List<Card> secondTeamPile = fillTeamPile();
        List<Card> firstTeamPile = fillTeamPile();
        secondTeamPile.addAll(fillTeamPile());
        PointsCalculator pointsCalculator = new PointsCalculator();
        int expectedHandingPoints = 0;
        int expectedFirstTeamPoints = 0;
        int expectedSecondTeamPoints = 15;
        initializePlayersAndTeams();

        int result = pointsCalculator.calculateTeamsPoints(firstTeam, secondTeam, firstTeamPile, secondTeamPile, new AllTrumpsRules(), thirdPlayer, firstPlayer, false, false, 0,
                new ArrayList<>(), new ArrayList<>(), 0, 0);

        Assert.assertEquals(expectedHandingPoints, result);
        Assert.assertEquals(expectedFirstTeamPoints, firstTeam.getTeamPoints());
        Assert.assertEquals(expectedSecondTeamPoints, secondTeam.getTeamPoints());
    }

    @Test
    public void calculateTeamsPoints_returnFiftyFive_whenHandingPoints() {
        List<Card> secondTeamPile = fillTeamPile();
        List<Card> firstTeamPile = fillTeamPile();
        firstTeamPile.add(new Card(Rank.DIAMONDS, Suit.TEN, 10, 10));
        PointsCalculator pointsCalculator = new PointsCalculator();
        int expectedHandingPoints = 55;
        int expectedFirstTeamPoints = 0;
        int expectedSecondTeamPoints = 6;
        initializePlayersAndTeams();

        int result = pointsCalculator.calculateTeamsPoints(firstTeam, secondTeam, firstTeamPile, secondTeamPile, new AllTrumpsRules(), thirdPlayer, firstPlayer, false, false, 0,
                new ArrayList<>(), new ArrayList<>(), 0, 0);

        Assert.assertEquals(expectedHandingPoints, result);
        Assert.assertEquals(expectedFirstTeamPoints, firstTeam.getTeamPoints());
        Assert.assertEquals(expectedSecondTeamPoints, secondTeam.getTeamPoints());
    }

    @Test
    public void calculateTeamsPoints_returnZero_whenHandingPointsFromLastRound() {
        List<Card> secondTeamPile = fillTeamPile();
        List<Card> firstTeamPile = fillTeamPile();
        PointsCalculator pointsCalculator = new PointsCalculator();
        int expectedHandingPoints = 0;
        int expectedFirstTeamPoints = 0;
        int expectedSecondTeamPoints = 11;
        initializePlayersAndTeams();

        int result = pointsCalculator.calculateTeamsPoints(firstTeam, secondTeam, firstTeamPile, secondTeamPile, new AllTrumpsRules(), thirdPlayer, firstPlayer, false, false, 10,
                new ArrayList<>(), new ArrayList<>(), 0, 0);

        Assert.assertEquals(expectedHandingPoints, result);
        Assert.assertEquals(expectedFirstTeamPoints, firstTeam.getTeamPoints());
        Assert.assertEquals(expectedSecondTeamPoints, secondTeam.getTeamPoints());
    }

    @Test
    public void calculateTeamsPoints_returnZero_whenHandingPointsAndHandingPointsFromLastRound() {
        List<Card> secondTeamPile = fillTeamPile();
        List<Card> firstTeamPile = fillTeamPile();
        firstTeamPile.add(new Card(Rank.DIAMONDS, Suit.TEN, 10, 10));
        PointsCalculator pointsCalculator = new PointsCalculator();
        int expectedHandingPoints = 230;
        int expectedFirstTeamPoints = 0;
        int expectedSecondTeamPoints = 0;
        initializePlayersAndTeams();

        int result = pointsCalculator.calculateTeamsPoints(firstTeam, secondTeam, firstTeamPile, secondTeamPile, new AllTrumpsRules(), thirdPlayer, firstPlayer, true, false, 10,
                new ArrayList<>(), new ArrayList<>(), 0, 0);

        Assert.assertEquals(expectedHandingPoints, result);
        Assert.assertEquals(expectedFirstTeamPoints, firstTeam.getTeamPoints());
        Assert.assertEquals(expectedSecondTeamPoints, secondTeam.getTeamPoints());
    }

    private CrawlTeams initializePlayersAndTeams() {
        firstPlayer = new VirtualPlayer("first");
        secondPlayer = new VirtualPlayer("second");
        thirdPlayer = new VirtualPlayer("third");
        fourthPlayer = new VirtualPlayer("fourth");

        firstTeam = new Team();
        firstTeam.addPlayer(firstPlayer, 1);
        firstTeam.addPlayer(secondPlayer);

        secondTeam = new Team();
        secondTeam.addPlayer(thirdPlayer, 1);
        secondTeam.addPlayer(fourthPlayer);

        return new CrawlTeams(firstTeam, secondTeam, 4);
    }

    private List<Card> fillTeamPile() {
        Card sevenCard = new Card(Rank.HEARTS, Suit.SEVEN, 0, 0);
        Card jackCard = new Card(Rank.SPADES, Suit.JACK, 20, 2);
        Card nineCard = new Card(Rank.HEARTS, Suit.NINE, 14, 0);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);

        return new ArrayList<>(Arrays.asList(sevenCard, jackCard, nineCard, aceCard));
    }
}