package com.vs.belote.game.bids;

import com.vs.belote.game.auxiliaryclasses.Pair;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.team.CrawlTeams;
import com.vs.belote.game.team.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

public class BidVoterTest {
    private Player firstPlayer;
    private Player secondPlayer;
    private Player thirdPlayer;
    private Player fourthPlayer;
    private CrawlTeams crawlTeams;

    @Before
    public void mockPlayersAndTeams() {
        firstPlayer = Mockito.mock(Player.class);
        secondPlayer = Mockito.mock(Player.class);
        thirdPlayer = Mockito.mock(Player.class);
        fourthPlayer = Mockito.mock(Player.class);
        Team firstTeam = new Team();
        firstTeam.addPlayer(firstPlayer, 1);
        firstTeam.addPlayer(thirdPlayer);
        Team secondTeam = new Team();
        secondTeam.addPlayer(secondPlayer, 1);
        secondTeam.addPlayer(fourthPlayer);
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 2);
    }

    @Test
    public void bid_returnDiamond_whenBidsArePassWithDiamonds() {
        setCalls(
                new Bid[]{Bid.PASS, Bid.PASS},
                firstPlayer,
                new Bid[]{null, Matchers.any(Bid.class)},
                new Boolean[]{true, Matchers.anyBoolean()}
        );
        setCalls(
                new Bid[]{Bid.PASS, Bid.PASS},
                secondPlayer, new Bid[]{null, Matchers.any(Bid.class)},
                new Boolean[]{false, Matchers.anyBoolean()}
        );
        setCalls(
                new Bid[]{Bid.SPADES},
                thirdPlayer, new Bid[]{Matchers.any(Bid.class)},
                new Boolean[]{Matchers.anyBoolean()}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                fourthPlayer, new Bid[]{Matchers.any(Bid.class)},
                new Boolean[]{Matchers.anyBoolean()}
        );
        Player expectedBidWinner = thirdPlayer;
        Bid expectedBid = Bid.SPADES;

        Pair<Player, Bid> result = BidVoter.bid(crawlTeams.getNextPlayer(fourthPlayer), crawlTeams);

        Assert.assertEquals(expectedBidWinner, result.getKey());
        Assert.assertEquals(expectedBid, result.getValue());
    }

    @Test
    public void bid_returnNull_whenBidsArePassOnly() {
        setCalls(
                new Bid[]{Bid.PASS},
                firstPlayer,
                new Bid[]{null},
                new Boolean[]{false}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                secondPlayer,
                new Bid[]{null},
                new Boolean[]{false}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                thirdPlayer,
                new Bid[]{null},
                new Boolean[]{false}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                fourthPlayer,
                new Bid[]{null},
                new Boolean[]{false}
        );

        Pair<Player, Bid> result = BidVoter.bid(crawlTeams.getNextPlayer(fourthPlayer), crawlTeams);

        Assert.assertNull(result.getKey());
        Assert.assertNull(result.getValue());
    }

    @Test
    public void bid_returnBid_whenBidsAnd3xPass() {
        setCalls(
                new Bid[]{Bid.SPADES},
                firstPlayer,
                new Bid[]{null},
                new Boolean[]{false}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                secondPlayer,
                new Bid[]{Bid.SPADES},
                new Boolean[]{false}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                thirdPlayer,
                new Bid[]{Bid.SPADES},
                new Boolean[]{true}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                fourthPlayer,
                new Bid[]{Bid.SPADES},
                new Boolean[]{false}
        );
        Pair<Player, Bid> expected = new Pair<>(firstPlayer, Bid.SPADES);

        Pair<Player, Bid> result = BidVoter.bid(fourthPlayer, crawlTeams);

        Assert.assertEquals(expected.getKey(), result.getKey());
        Assert.assertEquals(expected.getValue(), result.getValue());
    }

    @Test
    public void bid_returnBidWithDouble_whenBidsAndAndDouble() {
        setCalls(
                new Bid[]{Bid.DIAMONDS, Bid.PASS},
                firstPlayer,
                new Bid[]{null, Bid.DIAMONDS},
                new Boolean[]{false, false}
        );
        Bid bid = Bid.DIAMONDS;
        bid.setDoublePoints(true);
        setCalls(
                new Bid[]{bid},
                secondPlayer,
                new Bid[]{Bid.DIAMONDS},
                new Boolean[]{false}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                thirdPlayer,
                new Bid[]{Bid.DIAMONDS},
                new Boolean[]{false}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                fourthPlayer,
                new Bid[]{Bid.DIAMONDS},
                new Boolean[]{true}
        );
        Pair<Player, Bid> expected = new Pair<>(secondPlayer, bid);

        Pair<Player, Bid> result = BidVoter.bid(fourthPlayer, crawlTeams);

        Assert.assertEquals(expected.getKey(), result.getKey());
        Assert.assertEquals(expected.getValue(), result.getValue());
        Assert.assertTrue(result.getValue().isDoublePoints());
    }

    @Test
    public void bid_returnBidWithReDouble_whenBidAndDoubleAndReDouble() {
        Bid anotherBid = Bid.HEARTHS;
        anotherBid.setReDoublePoints(true);
        setCalls(
                new Bid[]{Bid.HEARTHS, anotherBid},
                firstPlayer,
                new Bid[]{null, Bid.HEARTHS},
                new Boolean[]{false, false}
        );
        Bid bid = Bid.HEARTHS;
        bid.setDoublePoints(true);
        setCalls(
                new Bid[]{bid, Bid.PASS},
                secondPlayer,
                new Bid[]{Bid.HEARTHS, Bid.HEARTHS},
                new Boolean[]{false, false}
        );
        setCalls(
                new Bid[]{Bid.PASS, Bid.PASS},
                thirdPlayer,
                new Bid[]{Bid.HEARTHS, Bid.HEARTHS},
                new Boolean[]{false, true}
        );
        setCalls(
                new Bid[]{Bid.PASS, Bid.PASS},
                fourthPlayer,
                new Bid[]{Bid.HEARTHS, Bid.HEARTHS},
                new Boolean[]{true, false}
        );
        Pair<Player, Bid> expected = new Pair<>(firstPlayer, bid);

        Pair<Player, Bid> result = BidVoter.bid(fourthPlayer, crawlTeams);

        Assert.assertEquals(expected.getKey(), result.getKey());
        Assert.assertEquals(expected.getValue(), result.getValue());
        Assert.assertTrue(result.getValue().isDoublePoints());
    }

    @Test
    public void bid_returnHighestBid_whenBidAndBid() {
        setCalls(
                new Bid[]{Bid.SPADES, Bid.PASS},
                firstPlayer,
                new Bid[]{null, Bid.CLUBS},
                new Boolean[]{false, false}
        );
        setCalls(
                new Bid[]{Bid.CLUBS},
                secondPlayer,
                new Bid[]{Bid.SPADES},
                new Boolean[]{false}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                thirdPlayer,
                new Bid[]{Bid.CLUBS},
                new Boolean[]{false}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                fourthPlayer,
                new Bid[]{Bid.CLUBS},
                new Boolean[]{true}
        );
        Pair<Player, Bid> expected = new Pair<>(secondPlayer, Bid.CLUBS);

        Pair<Player, Bid> result = BidVoter.bid(fourthPlayer, crawlTeams);

        Assert.assertEquals(expected.getKey(), result.getKey());
        Assert.assertEquals(expected.getValue(), result.getValue());
    }

    @Test
    public void bid_returnHighestBid_whenBidAndBidAndBid() {
        setCalls(
                new Bid[]{Bid.SPADES, Bid.PASS},
                firstPlayer,
                new Bid[]{null, Bid.ALLTRUMPS},
                new Boolean[]{false, true}
        );
        setCalls(
                new Bid[]{Bid.CLUBS, Bid.PASS},
                secondPlayer,
                new Bid[]{Bid.SPADES, Bid.ALLTRUMPS},
                new Boolean[]{false, false}
        );
        setCalls(
                new Bid[]{Bid.ALLTRUMPS},
                thirdPlayer,
                new Bid[]{Bid.CLUBS},
                new Boolean[]{false}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                fourthPlayer,
                new Bid[]{Bid.ALLTRUMPS},
                new Boolean[]{false}
        );
        Pair<Player, Bid> expected = new Pair<>(thirdPlayer, Bid.ALLTRUMPS);

        Pair<Player, Bid> result = BidVoter.bid(fourthPlayer, crawlTeams);

        Assert.assertEquals(expected.getKey(), result.getKey());
        Assert.assertEquals(expected.getValue(), result.getValue());
    }

    @Test
    public void bid_returnHighestBidWithDouble_whenBidAndBidAndBidAndDouble() {
        setCalls(
                new Bid[]{Bid.SPADES, Bid.PASS},
                firstPlayer,
                new Bid[]{null, Bid.CLUBS},
                new Boolean[]{false, true}
        );
        setCalls(
                new Bid[]{Bid.CLUBS, Bid.PASS},
                secondPlayer,
                new Bid[]{Bid.SPADES, Bid.CLUBS},
                new Boolean[]{false, false}
        );
        Bid bid = Bid.CLUBS;
        bid.setDoublePoints(true);
        setCalls(
                new Bid[]{bid},
                thirdPlayer,
                new Bid[]{Bid.CLUBS},
                new Boolean[]{false}
        );
        setCalls(
                new Bid[]{Bid.PASS},
                fourthPlayer,
                new Bid[]{Bid.CLUBS},
                new Boolean[]{false}
        );
        Pair<Player, Bid> expected = new Pair<>(thirdPlayer, Bid.CLUBS);

        Pair<Player, Bid> result = BidVoter.bid(fourthPlayer, crawlTeams);

        Assert.assertEquals(expected.getKey(), result.getKey());
        Assert.assertEquals(expected.getValue(), result.getValue());
        Assert.assertTrue(result.getValue().isDoublePoints());
    }

    //Philip should see this
    private void setCalls(Bid[] returnValues, Player player, Bid[] highestBids, Boolean[] arePlayersFromThisTeam) {
        for (int i = 0; i < returnValues.length; i++) {
            Mockito.when(player.makeABid(highestBids[i], arePlayersFromThisTeam[i])).thenReturn(returnValues[i]);
        }
    }

}