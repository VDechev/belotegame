package com.vs.belote.game.rules;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;
import com.vs.belote.game.deck.Suit;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.players.VirtualPlayer;
import com.vs.belote.game.team.CrawlTeams;
import com.vs.belote.game.team.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NoTrumpsRulesTest {
    private NoTrumpsRules rule;
    private Player secondPlayer;
    private Player thirdPlayer;
    private Player fourthPlayer;

    @Before
    public void initializePlayersAndRule() {
        rule = new NoTrumpsRules();
        secondPlayer = new VirtualPlayer("second");
        thirdPlayer = new VirtualPlayer("third");
        fourthPlayer = new VirtualPlayer("fourth");
    }

    @Test
    public void moveWinner_returnThirdPlayer_whenAceCardPlayed() {
        Player expectedWinner = new VirtualPlayer("");
        CrawlTeams crawlTeams = initializePlayersAndTeams(expectedWinner);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        Card kingCard = new Card(Rank.HEARTS, Suit.KING, 4, 4);
        Card queenCard = new Card(Rank.SPADES, Suit.QUEEN, 3, 3);
        Card tenCard = new Card(Rank.HEARTS, Suit.TEN, 10, 10);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(aceCard, kingCard, queenCard, tenCard));

        Player result = rule.moveWinner(moveCards, crawlTeams, expectedWinner);

        Assert.assertEquals(thirdPlayer, result);
    }

    @Test
    public void canIPlayThisCard_returnTrue_whenHigherCardIsGiven() {
        Player player = new VirtualPlayer("");
        CrawlTeams crawlTeams = initializePlayersAndTeams(player);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        fillHand(player);
        Card kingCard = new Card(Rank.HEARTS, Suit.KING, 4, 4);
        Card queenCard = new Card(Rank.SPADES, Suit.QUEEN, 3, 3);
        Card tenCard = new Card(Rank.HEARTS, Suit.TEN, 10, 10);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(kingCard, queenCard, tenCard));

        boolean result = rule.canIPlayThisCard(moveCards, aceCard, player.getHand(), crawlTeams, secondPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void canIPlayThisCard_returnTrue_whenAnswer() {
        Player player = new VirtualPlayer("");
        CrawlTeams crawlTeams = initializePlayersAndTeams(player);
        Card nineCard = new Card(Rank.HEARTS, Suit.NINE, 11, 11);
        fillHand(player);
        Card kingCard = new Card(Rank.HEARTS, Suit.KING, 4, 4);
        Card queenCard = new Card(Rank.SPADES, Suit.QUEEN, 3, 3);
        Card tenCard = new Card(Rank.HEARTS, Suit.TEN, 10, 10);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(kingCard, queenCard, tenCard));

        boolean result = rule.canIPlayThisCard(moveCards, nineCard, player.getHand(), crawlTeams, secondPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void canIPlayThisCard_returnTrue_whenDifferentRankCardIsGivenAndCantAnswer() {
        Player player = new VirtualPlayer("");
        CrawlTeams crawlTeams = initializePlayersAndTeams(player);
        Card nineCard = new Card(Rank.CLUBS, Suit.NINE, 11, 11);
        fillHand(player);
        Card kingCard = new Card(Rank.HEARTS, Suit.KING, 4, 4);
        Card queenCard = new Card(Rank.SPADES, Suit.QUEEN, 3, 3);
        Card tenCard = new Card(Rank.HEARTS, Suit.TEN, 10, 10);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(kingCard, queenCard, tenCard));

        boolean result = rule.canIPlayThisCard(moveCards, nineCard, player.getHand(), crawlTeams, thirdPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void calculatePoints_returnFour_whenTwoJacksAreGiven() {
        Card jackSpadesCard = new Card(Rank.SPADES, Suit.JACK, 20, 2);
        Card jackDiamondsCard = new Card(Rank.DIAMONDS, Suit.JACK, 20, 2);
        List<Card> cardsForCalculating = new ArrayList<>(Arrays.asList(jackDiamondsCard, jackSpadesCard));
        int expected = 8;

        int result = rule.calculatePoints(cardsForCalculating);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void calculatePoints_returnPointsSum_whenDifferentCardsAreGiven() {
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        Card kingCard = new Card(Rank.HEARTS, Suit.KING, 4, 4);
        Card queenCard = new Card(Rank.SPADES, Suit.QUEEN, 3, 3);
        Card tenCard = new Card(Rank.HEARTS, Suit.TEN, 10, 10);
        List<Card> cardsForCalculating = new ArrayList<>(Arrays.asList(aceCard, kingCard, queenCard, tenCard));
        int expected = 56;

        int result = rule.calculatePoints(cardsForCalculating);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void roundPoints_returnSixteen_whenOneHundredAndFortySixInputIsGiven() {
        int pointsForRounding = 156;
        int expected = 16;

        int result = rule.roundPoints(pointsForRounding);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void roundPoints_returnSixteen_whenOneHundredAndFortyFiveInputIsGiven() {
        int pointsForRounding = 155;
        int expected = 16;

        int result = rule.roundPoints(pointsForRounding);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void roundPoints_returnFifteen_whenOneHundredAndFortyFourInputIsGiven() {
        int pointsForRounding = 154;
        int expected = 15;

        int result = rule.roundPoints(pointsForRounding);

        Assert.assertEquals(expected, result);
    }


    private CrawlTeams initializePlayersAndTeams(Player player) {
        Team firstTeam = new Team();
        firstTeam.addPlayer(player, 1);
        firstTeam.addPlayer(secondPlayer);

        Team secondTeam = new Team();
        secondTeam.addPlayer(thirdPlayer, 1);
        secondTeam.addPlayer(fourthPlayer);

        return new CrawlTeams(firstTeam, secondTeam, 4);
    }

    private void fillHand(Player humanPlayer) {
        Card first = new Card(Rank.DIAMONDS, Suit.KING, 4, 4);
        Card second = new Card(Rank.DIAMONDS, Suit.JACK, 20, 2);
        Card third = new Card(Rank.DIAMONDS, Suit.ACE, 11, 11);
        humanPlayer.addCardsToHand(new ArrayList<>(Arrays.asList(first, second, third)));
    }
}