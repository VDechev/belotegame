package com.vs.belote.game.rules;

import com.vs.belote.game.bids.Bid;
import org.junit.Assert;
import org.junit.Test;

public class RuleSwitcherTest {

    @Test
    public void getRulesViaBid_returnAllTrumps_whenBidIsAllTrumps() {
        GameRules expected = new AllTrumpsRules();

        GameRules result = RuleSwitcher.getRulesViaBid(Bid.ALLTRUMPS);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void getRulesViaBid_returnNoTrump_whenBidIsNoTrumps() {
        GameRules expected = new NoTrumpsRules();

        GameRules result = RuleSwitcher.getRulesViaBid(Bid.NOTRUMPS);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void getRulesViaBid_returnRankRule_whenBidIsDiamonds() {
        GameRules expected = new RankRules(Bid.DIAMONDS);

        GameRules result = RuleSwitcher.getRulesViaBid(Bid.DIAMONDS);

        Assert.assertEquals(expected, result);
    }

}