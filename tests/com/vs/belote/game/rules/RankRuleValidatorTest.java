package com.vs.belote.game.rules;

import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;
import com.vs.belote.game.deck.Suit;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.players.VirtualPlayer;
import com.vs.belote.game.team.CrawlTeams;
import com.vs.belote.game.team.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RankRuleValidatorTest {

    private Player secondPlayer;
    private Player thirdPlayer;
    private Player fourthPlayer;

    @Before
    public void initializePlayers() {
        secondPlayer = new VirtualPlayer("second");
        thirdPlayer = new VirtualPlayer("third");
        fourthPlayer = new VirtualPlayer("fourth");
    }

    @Test
    public void getMoveCardIndexFromCard_returnTwo_whenMoveCardsAreGiven() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        Card kingCard = new Card(Rank.HEARTS, Suit.KING, 4, 4);
        Card queenCard = new Card(Rank.SPADES, Suit.QUEEN, 3, 3);
        Card tenCard = new Card(Rank.HEARTS, Suit.TEN, 10, 10);
        List<Card> cards = new ArrayList<>(Arrays.asList(aceCard, kingCard, queenCard, tenCard));
        int expected = 2;

        int result = validator.getMoveCardIndexFromCard(queenCard, cards);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void getMoveCardIndexFromCard_returnMinusOne_whenMoveCardsAreGiven() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        Card kingCard = new Card(Rank.HEARTS, Suit.KING, 4, 4);
        Card queenCard = new Card(Rank.SPADES, Suit.QUEEN, 3, 3);
        Card tenCard = new Card(Rank.HEARTS, Suit.TEN, 10, 10);
        Card jackCard = new Card(Rank.DIAMONDS, Suit.JACK, 20, 2);
        List<Card> cards = new ArrayList<>(Arrays.asList(aceCard, kingCard, queenCard, tenCard));
        int expected = -1;

        int result = validator.getMoveCardIndexFromCard(jackCard, cards);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void checkIfCardIsCorrect_returnTrue_whenAnswer() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.DIAMONDS, Suit.ACE, 11, 11);
        Player current = new VirtualPlayer("current");
        current.addCardsToHand(new ArrayList<>(Collections.singleton(aceCard)));
        Card nineCard = new Card(Rank.DIAMONDS, Suit.NINE, 14, 0);
        List<Card> moveCards = new ArrayList<>(Collections.singleton(nineCard));
        CrawlTeams crawlTeams = initializePlayersAndTeams(current);

        boolean result = validator.checkIfCardIsCorrect(moveCards, aceCard, current.getHand(), crawlTeams, fourthPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void checkIfCardIsCorrect_returnTrue_whenOverrun() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.DIAMONDS, Suit.ACE, 11, 11);
        Player current = new VirtualPlayer("current");
        current.addCardsToHand(new ArrayList<>(Collections.singleton(aceCard)));
        Card nineCard = new Card(Rank.DIAMONDS, Suit.NINE, 14, 0);
        List<Card> moveCards = new ArrayList<>(Collections.singleton(nineCard));
        CrawlTeams crawlTeams = initializePlayersAndTeams(current);

        boolean result = validator.checkIfCardIsCorrect(moveCards, aceCard, current.getHand(), crawlTeams, fourthPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void checkIfCardIsCorrect_returnTrue_whenTrumping() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.SPADES, Suit.ACE, 11, 11);
        Card nineCard = new Card(Rank.DIAMONDS, Suit.NINE, 14, 0);
        List<Card> moveCards = new ArrayList<>(Collections.singleton(nineCard));
        Player current = new VirtualPlayer("current");
        current.addCardsToHand(new ArrayList<>(Collections.singleton(aceCard)));
        CrawlTeams crawlTeams = initializePlayersAndTeams(current);

        boolean result = validator.checkIfCardIsCorrect(moveCards, aceCard, current.getHand(), crawlTeams, fourthPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void checkIfCardIsCorrect_returnTrue_whenDifferentRankInCaseCantAnswerOrTrump() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.CLUBS, Suit.ACE, 11, 11);
        Player current = new VirtualPlayer("current");
        current.addCardsToHand(new ArrayList<>(Collections.singleton(aceCard)));
        Card nineCard = new Card(Rank.DIAMONDS, Suit.NINE, 14, 0);
        List<Card> moveCards = new ArrayList<>(Collections.singleton(nineCard));
        CrawlTeams crawlTeams = initializePlayersAndTeams(current);

        boolean result = validator.checkIfCardIsCorrect(moveCards, aceCard, current.getHand(), crawlTeams, fourthPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void checkIfCardIsCorrect_returnTrue_whenDifferentRankWhenTeammateTrump() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.CLUBS, Suit.ACE, 11, 11);
        Card aceTrumpCard = new Card(Rank.SPADES, Suit.ACE, 11, 11);
        Player current = new VirtualPlayer("current");
        current.addCardsToHand(new ArrayList<>(Arrays.asList(aceCard, aceTrumpCard)));

        Card nineCard = new Card(Rank.DIAMONDS, Suit.NINE, 14, 0);
        Card tenTrumpCard = new Card(Rank.SPADES, Suit.TEN, 10, 10);
        Card jackCard = new Card(Rank.DIAMONDS, Suit.JACK, 20, 2);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(nineCard, tenTrumpCard, jackCard));
        CrawlTeams crawlTeams = initializePlayersAndTeams(current);

        boolean result = validator.checkIfCardIsCorrect(moveCards, aceCard, current.getHand(), crawlTeams, thirdPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void checkIfCardIsCorrect_returnTrue_whenOverrunTrump() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.CLUBS, Suit.ACE, 11, 11);
        Card aceTrumpCard = new Card(Rank.SPADES, Suit.ACE, 11, 11);
        Player current = new VirtualPlayer("current");
        current.addCardsToHand(new ArrayList<>(Arrays.asList(aceCard, aceTrumpCard)));

        Card nineCard = new Card(Rank.DIAMONDS, Suit.NINE, 14, 0);
        Card tenTrumpCard = new Card(Rank.SPADES, Suit.TEN, 10, 10);
        Card jackCard = new Card(Rank.DIAMONDS, Suit.JACK, 20, 2);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(nineCard, tenTrumpCard, jackCard));
        CrawlTeams crawlTeams = initializePlayersAndTeams(current);

        boolean result = validator.checkIfCardIsCorrect(moveCards, aceTrumpCard, current.getHand(), crawlTeams, thirdPlayer);

        Assert.assertTrue(result);
    }

    @Test
    public void checkIfCardIsCorrect_returnFalse_whenNotAnswerIfCanAnswer() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.CLUBS, Suit.ACE, 11, 11);
        Card sevenCard = new Card(Rank.DIAMONDS, Suit.ACE, 11, 11);
        Player current = new VirtualPlayer("current");
        current.addCardsToHand(new ArrayList<>(Arrays.asList(aceCard, sevenCard)));

        Card nineCard = new Card(Rank.DIAMONDS, Suit.NINE, 14, 0);
        Card tenCard = new Card(Rank.DIAMONDS, Suit.TEN, 10, 10);
        Card jackCard = new Card(Rank.DIAMONDS, Suit.JACK, 20, 2);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(nineCard, tenCard, jackCard));
        CrawlTeams crawlTeams = initializePlayersAndTeams(current);

        boolean result = validator.checkIfCardIsCorrect(moveCards, aceCard, current.getHand(), crawlTeams, thirdPlayer);

        Assert.assertFalse(result);
    }

    @Test
    public void checkIfCardIsCorrect_returnFalse_whenTrumpingWhenCanAnswer() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.SPADES, Suit.ACE, 11, 11);
        Card sevenCard = new Card(Rank.DIAMONDS, Suit.ACE, 11, 11);
        Player current = new VirtualPlayer("current");
        current.addCardsToHand(new ArrayList<>(Arrays.asList(aceCard, sevenCard)));

        Card nineCard = new Card(Rank.DIAMONDS, Suit.NINE, 14, 0);
        Card tenCard = new Card(Rank.DIAMONDS, Suit.TEN, 10, 10);
        Card jackCard = new Card(Rank.DIAMONDS, Suit.JACK, 20, 2);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(nineCard, tenCard, jackCard));
        CrawlTeams crawlTeams = initializePlayersAndTeams(current);

        boolean result = validator.checkIfCardIsCorrect(moveCards, aceCard, current.getHand(), crawlTeams, thirdPlayer);

        Assert.assertFalse(result);
    }

    @Test
    public void checkIfCardIsCorrect_returnFalse_whenNotAnswerTrumpIfCanAnswer() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.SPADES, Suit.ACE, 11, 11);
        Card sevenCard = new Card(Rank.DIAMONDS, Suit.ACE, 11, 11);
        Player current = new VirtualPlayer("current");
        current.addCardsToHand(new ArrayList<>(Arrays.asList(aceCard, sevenCard)));

        Card nineCard = new Card(Rank.DIAMONDS, Suit.NINE, 14, 0);
        Card tenCard = new Card(Rank.DIAMONDS, Suit.TEN, 10, 10);
        Card jackCard = new Card(Rank.DIAMONDS, Suit.JACK, 20, 2);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(nineCard, tenCard, jackCard));
        CrawlTeams crawlTeams = initializePlayersAndTeams(current);

        boolean result = validator.checkIfCardIsCorrect(moveCards, aceCard, current.getHand(), crawlTeams, thirdPlayer);

        Assert.assertFalse(result);
    }

    @Test
    public void checkIfCardIsCorrect_returnFalse_whenNotOverrunTrumpIfCan() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.SPADES, Suit.ACE, 11, 11);
        Card jackCard = new Card(Rank.SPADES, Suit.JACK, 20, 2);
        Player current = new VirtualPlayer("current");
        current.addCardsToHand(new ArrayList<>(Arrays.asList(aceCard, jackCard)));

        Card nineCard = new Card(Rank.DIAMONDS, Suit.NINE, 14, 0);
        Card tenCard = new Card(Rank.DIAMONDS, Suit.TEN, 10, 10);
        Card nineTrumpCard = new Card(Rank.SPADES, Suit.NINE, 14, 0);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(nineCard, tenCard, nineTrumpCard));
        CrawlTeams crawlTeams = initializePlayersAndTeams(current);

        boolean result = validator.checkIfCardIsCorrect(moveCards, aceCard, current.getHand(), crawlTeams, thirdPlayer);

        Assert.assertFalse(result);
    }

    @Test
    public void getHighestCardFromTrumpMove_returnHighestCard_whenCardsAreOnlyTrumps() {
        RankRuleValidator validator = new RankRuleValidator(Bid.SPADES);
        Card aceCard = new Card(Rank.SPADES, Suit.ACE, 11, 11);
        Card jackCard = new Card(Rank.SPADES, Suit.JACK, 20, 2);
        Card nineCard = new Card(Rank.SPADES, Suit.NINE, 14, 0);
        Card sevenCard = new Card(Rank.SPADES, Suit.SEVEN, 0, 0);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(aceCard, jackCard, nineCard, sevenCard));

        Card result = validator.getHighestCardFromTrumpMove(moveCards);

        Assert.assertEquals(jackCard, result);
    }

    @Test
    public void getHighestCardFromNoTrumpMove_returnHighestCard_whenCardsAreNotOnlyTrumps() {
        RankRuleValidator validator = new RankRuleValidator(Bid.CLUBS);
        Card aceCard = new Card(Rank.SPADES, Suit.ACE, 11, 11);
        Card jackCard = new Card(Rank.SPADES, Suit.JACK, 20, 2);
        Card nineCard = new Card(Rank.SPADES, Suit.NINE, 14, 0);
        Card sevenCard = new Card(Rank.SPADES, Suit.SEVEN, 0, 0);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(aceCard, jackCard, nineCard, sevenCard));

        Card result = validator.getHighestCardIndexWhenNoTrumpMove(moveCards);

        Assert.assertEquals(aceCard, result);
    }

    private CrawlTeams initializePlayersAndTeams(Player player) {
        Team firstTeam = new Team();
        firstTeam.addPlayer(player, 1);
        firstTeam.addPlayer(secondPlayer);

        Team secondTeam = new Team();
        secondTeam.addPlayer(thirdPlayer, 1);
        secondTeam.addPlayer(fourthPlayer);

        return new CrawlTeams(firstTeam, secondTeam, 4);
    }
}