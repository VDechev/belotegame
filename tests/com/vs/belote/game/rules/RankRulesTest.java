package com.vs.belote.game.rules;

import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;
import com.vs.belote.game.deck.Suit;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.players.VirtualPlayer;
import com.vs.belote.game.team.CrawlTeams;
import com.vs.belote.game.team.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RankRulesTest {
    private Player secondPlayer;
    private Player thirdPlayer;
    private Player fourthPlayer;

    @Before
    public void initializePlayersAndRule() {
        secondPlayer = new VirtualPlayer("second");
        thirdPlayer = new VirtualPlayer("third");
        fourthPlayer = new VirtualPlayer("fourth");
    }

    @Test
    public void roundPoints_returnSixteen_whenOneHundredAndFortySixInputIsGiven() {
        int pointsForRounding = 156;
        int expected = 16;
        GameRules rule = new RankRules(Bid.SPADES);

        int result = rule.roundPoints(pointsForRounding);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void roundPoints_returnFifteen_whenOneHundredAndFortyFiveInputIsGiven() {
        int pointsForRounding = 155;
        int expected = 15;
        GameRules rule = new RankRules(Bid.SPADES);

        int result = rule.roundPoints(pointsForRounding);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void roundPoints_returnFifteen_whenOneHundredAndFortyFourInputIsGiven() {
        int pointsForRounding = 154;
        int expected = 15;
        GameRules rule = new RankRules(Bid.SPADES);

        int result = rule.roundPoints(pointsForRounding);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void moveWinner_returnThirdPlayer_jackCardIsPlayedInTrumpMove() {
        GameRules rule = new RankRules(Bid.HEARTHS);
        Player expectedWinner = new VirtualPlayer("");
        CrawlTeams crawlTeams = initializePlayersAndTeams(expectedWinner);
        Card sevenCard = new Card(Rank.HEARTS, Suit.SEVEN, 0, 0);
        Card jackCard = new Card(Rank.HEARTS, Suit.JACK, 20, 2);
        Card nineCard = new Card(Rank.HEARTS, Suit.NINE, 14, 0);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(sevenCard, jackCard, nineCard, aceCard));

        Player result = rule.moveWinner(moveCards, crawlTeams, expectedWinner);

        Assert.assertEquals(thirdPlayer, result);
    }

    @Test
    public void moveWinner_returnThirdPlayer_whenAcePlayedAndNoTrumpMove() {
        GameRules rule = new RankRules(Bid.SPADES);
        Player firstPlayer = new VirtualPlayer("first");
        CrawlTeams crawlTeams = initializePlayersAndTeams(firstPlayer);
        Card sevenCard = new Card(Rank.HEARTS, Suit.SEVEN, 0, 0);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        Card nineCard = new Card(Rank.HEARTS, Suit.NINE, 14, 0);
        Card jackCard = new Card(Rank.HEARTS, Suit.JACK, 20, 2);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(sevenCard, aceCard, nineCard, jackCard));

        Player result = rule.moveWinner(moveCards, crawlTeams, firstPlayer);

        Assert.assertEquals(thirdPlayer, result);
    }

    @Test
    public void moveWinner_returnThirdPlayer_whenTrumpAndNoTrumpCardsPlayed() { //have to check it
        GameRules rule = new RankRules(Bid.SPADES);
        Player expectedWinner = new VirtualPlayer("");
        CrawlTeams crawlTeams = initializePlayersAndTeams(expectedWinner);
        Card sevenCard = new Card(Rank.HEARTS, Suit.SEVEN, 0, 0);
        Card jackCard = new Card(Rank.SPADES, Suit.JACK, 20, 2);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        Card nineCard = new Card(Rank.HEARTS, Suit.NINE, 14, 0);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(sevenCard, jackCard, aceCard, nineCard));

        Player result = rule.moveWinner(moveCards, crawlTeams, expectedWinner);

        Assert.assertEquals(thirdPlayer, result);
    }

    @Test
    public void calculatePoints_returnFortyFive_whenOnlyTrumpsCardsAreGiven() {
        GameRules rule = new RankRules(Bid.HEARTHS);
        Card sevenCard = new Card(Rank.HEARTS, Suit.SEVEN, 0, 0);
        Card jackCard = new Card(Rank.HEARTS, Suit.JACK, 20, 2);
        Card nineCard = new Card(Rank.HEARTS, Suit.NINE, 14, 0);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(sevenCard, jackCard, nineCard, aceCard));
        int expected = 45;

        int result = rule.calculatePoints(moveCards);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void calculatePoints_returnThirteen_whenOnlyTrumpsCardsAreGiven() {
        GameRules rule = new RankRules(Bid.SPADES);
        Card sevenCard = new Card(Rank.HEARTS, Suit.SEVEN, 0, 0);
        Card jackCard = new Card(Rank.HEARTS, Suit.JACK, 20, 2);
        Card nineCard = new Card(Rank.HEARTS, Suit.NINE, 14, 0);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(sevenCard, jackCard, nineCard, aceCard));
        int expected = 13;

        int result = rule.calculatePoints(moveCards);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void calculatePoints_returnThirteen_whenTrumpAndNoTrumpCardsAreGiven() {
        GameRules rule = new RankRules(Bid.SPADES);
        Card sevenCard = new Card(Rank.HEARTS, Suit.SEVEN, 0, 0);
        Card jackCard = new Card(Rank.SPADES, Suit.JACK, 20, 2);
        Card nineCard = new Card(Rank.HEARTS, Suit.NINE, 14, 0);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        List<Card> moveCards = new ArrayList<>(Arrays.asList(sevenCard, jackCard, nineCard, aceCard));
        int expected = 31;

        int result = rule.calculatePoints(moveCards);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void canIPlayThisCard_returnTrue_whenNoCardsArePlayed() {
        GameRules rule = new RankRules(Bid.CLUBS);
        Player player = new VirtualPlayer("");
        CrawlTeams crawlTeams = initializePlayersAndTeams(player);
        Card aceCard = new Card(Rank.HEARTS, Suit.ACE, 11, 11);
        player.addCardsToHand(new ArrayList<>(Collections.singleton(aceCard)));

        boolean result = rule.canIPlayThisCard(null, aceCard, player.getHand(), crawlTeams, secondPlayer);

        Assert.assertTrue(result);
    }

    private CrawlTeams initializePlayersAndTeams(Player player) {
        Team firstTeam = new Team();
        firstTeam.addPlayer(player, 1);
        firstTeam.addPlayer(secondPlayer);

        Team secondTeam = new Team();
        secondTeam.addPlayer(thirdPlayer, 1);
        secondTeam.addPlayer(fourthPlayer);

        return new CrawlTeams(firstTeam, secondTeam, 4);
    }
}