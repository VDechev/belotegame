package com.vs.belote.game.deck;

import com.vs.belote.game.io.CommandLineIO;
import com.vs.belote.game.io.InputOutput;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class DeckReaderTest {
    private DeckReader deckReader;
    private ByteArrayOutputStream outContent;
    private InputOutput IO;


    public void setUpStreams() {
        outContent = new ByteArrayOutputStream();
        IO = mock(CommandLineIO.class);
        System.setOut(new PrintStream(outContent));
        deckReader = new DeckReader();
    }


    @Test
    public void createDeck_returnTrue_whenWrongFileGiven() {
        File wrongFile = new File("WrongBeloteCards.txt");
        setUpStreams();
        setWhenThen();
        setWhenThenForPrintLine("Can't open the current file");

        deckReader.createDeck(wrongFile, IO);

        Assert.assertTrue(outContent.toString().contains("Can't open the current file"));
    }

    @Test
    public void createDeck_returnTrue_whenFileWithCorruptedNoTrumpPointsGiven() {
        File wrongFile = new File("CorruptedNoTrumpsPointsFileForTest.txt");
        setUpStreams();
        setWhenThen();
        setWhenThenForPrintLine("Invalid cards information");

        deckReader.createDeck(wrongFile, IO);

        Assert.assertTrue(outContent.toString().contains("Invalid cards information"));
    }

    @Test
    public void createDeck_returnTrue_whenFileWithCorruptedAllTrumpPointsGiven() {
        File wrongFile = new File("CorruptedAllTrumpsPointsFileForTest.txt");
        setUpStreams();
        setWhenThen();
        setWhenThenForPrintLine("Invalid cards information");

        deckReader.createDeck(wrongFile, IO);

        Assert.assertTrue(outContent.toString().contains("Invalid cards information"));

    }

    @Test
    public void createDeck_returnTrue_whenFileWithCorruptedCardsNumberGiven() {
        File wrongFile = new File("CorruptedFileWithWrongCardsNumber.txt");
        setUpStreams();
        setWhenThen();
        setWhenThenForPrintLine("Invalid cards information");

        deckReader.createDeck(wrongFile, IO);

        Assert.assertTrue(outContent.toString().contains("Invalid cards information"));
    }

    @Test
    public void createDeck_returnTrue_whenEmptyFileGiven() {
        File wrongFile = new File("EmptyFileForTest.txt");
        setUpStreams();
        setWhenThen();
        setWhenThenForPrintLine("Invalid cards information");

        deckReader.createDeck(wrongFile, IO);

        Assert.assertTrue(outContent.toString().contains("Invalid cards information"));

    }

    private void setWhenThen() {
        when(IO.readLine()).thenReturn("Cards.txt");
    }

    private void setWhenThenForPrintLine(String out) {
        doAnswer(invocation -> {
            System.out.println(out);
            return null;
        }).when(IO).printLine(out);
    }

}