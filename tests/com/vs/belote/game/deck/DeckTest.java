package com.vs.belote.game.deck;

import com.vs.belote.game.io.CommandLineIO;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.players.VirtualPlayer;
import com.vs.belote.game.team.CrawlTeams;
import com.vs.belote.game.team.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.function.Predicate;

public class DeckTest {
    private File cardsFile;
    private Deck deck;
    private CrawlTeams crawlTeams;

    @Before
    public void createFile() {
        cardsFile = new File("Cards.txt");
        deck = new Deck(cardsFile, new CommandLineIO());
    }

    @Before
    public void initializeTeams() {
        Player firstPlayer = new VirtualPlayer("");
        Player secondPlayer = new VirtualPlayer("");
        Player thirdPlayer = new VirtualPlayer("");
        Player fourthPlayer = new VirtualPlayer("");
        Team firstTeam = new Team();
        firstTeam.addPlayer(secondPlayer, 1);
        firstTeam.addPlayer(firstPlayer);
        Team secondTeam = new Team();
        secondTeam.addPlayer(fourthPlayer, 2);
        secondTeam.addPlayer(thirdPlayer);
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, 1);
    }

    @Test
    public void getTwoRedAndTwoBlackCards_returnsListOfTwoRedAndTwoBlackCards() {
        List<Card> cards = deck.getTwoBlackAndTwoRedCards();

        Assert.assertEquals(2, cards.stream().filter(Card::isRed).toArray().length);
        Assert.assertEquals(2, cards.stream().filter(Predicate.not(Card::isRed)).toArray().length);
    }

    @Test
    public void dealToAllPlayers_deckSizeWillBe20_whenCalledOnce() {
        int result;
        int expected = 20;

        deck.dealToAllPlayers(crawlTeams, crawlTeams.getDealer());
        result = deck.deck.size();

        Assert.assertEquals(expected, result);
    }

    @Test
    public void dealToAllPlayers_deckSizeWillBe20_whenCalledTwice() {
        int result;
        int expected = 12;
        Player dealer = crawlTeams.getDealer();

        deck.dealToAllPlayers(crawlTeams, dealer);
        deck.dealToAllPlayers(crawlTeams, dealer);
        result = deck.deck.size();

        Assert.assertEquals(expected, result);
    }

    @Test
    public void dealToAllPlayers_DeckSizeWillBe20_whenCalledThrice() {
        int result;
        int expected = 0;
        Player dealer = crawlTeams.getDealer();

        deck.dealToAllPlayers(crawlTeams, dealer);
        deck.dealToAllPlayers(crawlTeams, dealer);
        deck.dealToAllPlayers(crawlTeams, dealer);
        result = deck.deck.size();

        Assert.assertEquals(expected, result);
    }
}