package com.vs.belote.game;

import com.vs.belote.game.team.Team;

public class Room {

    public Team playBeloteGame(BeloteTable beloteTable) {
        return beloteTable.playGameAndGetWinner();
    }
}
