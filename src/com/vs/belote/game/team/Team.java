package com.vs.belote.game.team;

import com.vs.belote.game.announcements.Announcement;
import com.vs.belote.game.deck.Card;
import com.vs.belote.game.players.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Team {
    private Player firstPlayer = null;
    private Player secondPlayer = null;
    private int teamPoints = 0;
    public List<Card> earnedCards;
    public List<Announcement> teamAnnouncements;
    public int numberOfBelotes = 0;

    public Team() {
        teamAnnouncements = new ArrayList<>();
        earnedCards = new ArrayList<>();
    }

    public boolean isTeamEmpty() {
        return firstPlayer == null && secondPlayer == null;
    }

    public void addPlayer(Player player, int seat) {
        if (seat == 1 && firstPlayer == null) {
            firstPlayer = player;
        } else if (seat == 2 && secondPlayer == null) {
            secondPlayer = player;
        }
    }

    public void addPlayer(Player player) {
        if (firstPlayer == null) {
            firstPlayer = player;
        } else if (secondPlayer == null) {
            secondPlayer = player;
        }
    }

    public void addPoints(int points) {
        teamPoints += points;
    }

    public Player getFirstPlayer() {
        return firstPlayer;
    }

    public Player getSecondPlayer() {
        return secondPlayer;
    }

    public boolean isPlayerFromThisTeam(Player player) {
        if (player == null) {
            return false;
        }
        return player.equals(firstPlayer) || player.equals(secondPlayer);
    }

    public int getTeamPoints() {
        return teamPoints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return firstPlayer.equals(team.firstPlayer) &&
                secondPlayer.equals(team.secondPlayer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstPlayer, secondPlayer);
    }
}
