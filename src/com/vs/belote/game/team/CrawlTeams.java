package com.vs.belote.game.team;

import com.vs.belote.game.players.Player;

public class CrawlTeams {
    private Team firstTeam;
    private Team secondTeam;
    private int playerToDeal;

    public CrawlTeams(Team firstTeam, Team secondTeam, int firstDealerIndex) {
        this.firstTeam = firstTeam;
        this.secondTeam = secondTeam;
        this.playerToDeal = firstDealerIndex;
    }

    public Player getDealer() {
        return getPlayerByIndex(playerToDeal);
    }

    public Player getNextPlayerToDeal() {
        return getPlayerByIndex(++playerToDeal);
    }

    public Player getNextPlayer(Player player) {
        return getNextPlayerFromPlayer(player);
    }

    public Team getPlayersTeam(Player player) {
        if (player.equals(firstTeam.getFirstPlayer()) || player.equals(firstTeam.getSecondPlayer())) {
            return firstTeam;
        }
        return secondTeam;
    }

    public boolean arePlayersFromSameTeam(Player first, Player second) {
        return (firstTeam.isPlayerFromThisTeam(first) && firstTeam.isPlayerFromThisTeam(second)) ||
                secondTeam.isPlayerFromThisTeam(first) && secondTeam.isPlayerFromThisTeam(second);
    }

    private Player getNextPlayerFromPlayer(Player player) {
        if (player.equals(firstTeam.getFirstPlayer())) {
            return secondTeam.getFirstPlayer();
        } else if (player.equals(secondTeam.getFirstPlayer())) {
            return firstTeam.getSecondPlayer();
        } else if (player.equals(firstTeam.getSecondPlayer())) {
            return secondTeam.getSecondPlayer();
        } else {
            return firstTeam.getFirstPlayer();
        }
    }

    private Player getPlayerByIndex(int index) {
        if (index % 4 == 0) {
            return firstTeam.getFirstPlayer();
        } else if (index % 4 == 1) {
            return secondTeam.getFirstPlayer();
        } else if (index % 4 == 2) {
            return firstTeam.getFirstPlayer();
        } else {
            return secondTeam.getSecondPlayer();
        }
    }

}
