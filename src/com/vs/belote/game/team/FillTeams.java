package com.vs.belote.game.team;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.io.InputOutput;
import com.vs.belote.game.players.HumanPlayer;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.players.VirtualPlayer;

import java.util.List;


public class FillTeams {

    private Team teamWithRedCards;
    private Team teamWithBlackCards;

    public FillTeams(InputOutput IO, List<Card> cardsForTeams) {
        teamWithBlackCards = new Team();
        teamWithRedCards = new Team();
        initializePlayers(IO, cardsForTeams);
    }

    public Team getTeamWithBlackCards() {
        return teamWithBlackCards;
    }

    public Team getTeamWithRedCards() {
        return teamWithRedCards;
    }

    private void fillTeams(Player player, InputOutput IO, List<Card> cardsForTeams) {
        //True if card is red
        if (getCardColour(player, cardsForTeams)) {
            IO.printLine("Your card is red");
            placePlayerInTeam(teamWithRedCards, player, IO);
        } else {
            IO.printLine("Your card is black");
            placePlayerInTeam(teamWithBlackCards, player, IO);
        }
    }

    private void placePlayerInTeam(Team team, Player player, InputOutput IO) {
        if (team.isTeamEmpty()) {
            int seatNumber = player.askPlayerForAMove("Select a seat! Pick a number ", 1, 2);
            team.addPlayer(player, seatNumber);
        } else {
            team.addPlayer(player);
        }
    }

    //Returns true if card is RED
    private boolean getCardColour(Player player, List<Card> cardsForTeams) {
        int playerCardIndex = player.askPlayerForAMove("Select a card. Their rank will be used for making your team! Pick a number from ",
                1, cardsForTeams.size());
        return cardsForTeams.remove(playerCardIndex - 1).isRed();
    }

    private Player createPlayer(InputOutput IO) {
        String name = readPlayerName(IO);
        if (isVirtualPlayer(IO)) {
            return new VirtualPlayer(name);
        }
        return new HumanPlayer(name, IO);
    }

    private boolean isVirtualPlayer(InputOutput IO) {
        IO.printLine("Choose 1 for physical player and 2 for virtual player");
        return Integer.parseInt(IO.readLine()) == 2;
    }

    private String readPlayerName(InputOutput IO) {
        IO.printLine("What's your name?");
        return IO.readLine();
    }

    private void initializePlayers(InputOutput IO, List<Card> cardsForTeams) {
        for (int i = 0; i < 4; i++) {
            Player player = createPlayer(IO);
            fillTeams(player, IO, cardsForTeams);
        }
    }
}
