package com.vs.belote.game;

import com.vs.belote.game.announcements.Announcement;
import com.vs.belote.game.announcements.AnnouncementPointsCalculator;
import com.vs.belote.game.deck.Card;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.rules.GameRules;
import com.vs.belote.game.team.Team;

import java.util.List;

public class PointsCalculator {

    private int firstTeamPoints = 0;
    private int firstTeamAnnouncementPoints = 0;
    private int secondTeamPoints = 0;
    private int secondTeamAnnouncementPoints = 0;
    private int totalSecondTeamPoints = 0;
    private int totalFirstTeamPoints = 0;

    public int calculateTeamsPoints(Team firstTeam, Team secondTeam, List<Card> firstTeamPile,
                                    List<Card> secondTeamPile, GameRules currentRule, Player moveWinner,
                                    Player bidder, boolean isDouble, boolean isReDouble,
                                    int handingPointsFromLastRound, List<Announcement> firstTeamAnnouncements,
                                    List<Announcement> secondTeamAnnouncements, int firstTeamBelotePoints,
                                    int secondTeamBelotePoints) {

        addPointsFromPilesAndAnnouncements(firstTeamPile, secondTeamPile, currentRule, firstTeamAnnouncements, secondTeamAnnouncements,
                firstTeamBelotePoints, secondTeamBelotePoints); // need to accept
        addLastTen(firstTeam, secondTeam, moveWinner);
        int handingPoints;
        if (isDouble) {
            handingPoints = calculatePointsIfDouble(isReDouble);
        } else {
            handingPoints = calculatePointsNoDouble(bidder, firstTeam);
        }
        handingPoints += addPointsFromLastHanding(handingPointsFromLastRound);
        firstTeam.addPoints(currentRule.roundPoints(totalFirstTeamPoints));
        secondTeam.addPoints(currentRule.roundPoints(totalSecondTeamPoints));
        return handingPoints;
    }

    private int calculatePointsIfDouble(boolean isReDouble) {
        int multiply = 2;
        if (isReDouble) {
            multiply = 4;
        }
        int firstTeamTotalPoints = firstTeamAnnouncementPoints + firstTeamPoints;
        int secondTeamTotalPoints = secondTeamAnnouncementPoints + secondTeamPoints;
        if (firstTeamPoints == 0 || secondTeamPoints == 0) {
            return calculatePointsWhenValat();
        } else {
            return calculatePointsWhenDouble(firstTeamTotalPoints, secondTeamTotalPoints, multiply);
        }

    }

    private int calculatePointsWhenDouble(int firstTeamTotalPoints, int secondTeamTotalPoints, int multiply) {
        if (firstTeamTotalPoints > secondTeamTotalPoints) {
            totalFirstTeamPoints += firstTeamTotalPoints + secondTeamTotalPoints;
            totalFirstTeamPoints *= multiply;
            return 0;
        } else if (firstTeamTotalPoints < secondTeamTotalPoints) {
            totalSecondTeamPoints += firstTeamTotalPoints + secondTeamTotalPoints;
            totalSecondTeamPoints *= multiply;
            return 0;
        } else {
            return (firstTeamTotalPoints + secondTeamTotalPoints) * multiply;
        }
    }

    private int calculatePointsWhenValat() {
        if (firstTeamPoints == 0) {
            totalSecondTeamPoints += 90;
        } else {
            totalFirstTeamPoints += 90;
        }
        totalFirstTeamPoints += firstTeamAnnouncementPoints + firstTeamPoints;
        totalSecondTeamPoints += secondTeamAnnouncementPoints + secondTeamPoints;
        return 0;
    }

    private int calculatePointsNoDouble(Player bidder, Team firstTeam) {
        int firstTeamTotalPoints = firstTeamAnnouncementPoints + firstTeamPoints;
        int secondTeamTotalPoints = secondTeamAnnouncementPoints + secondTeamPoints;
        if (firstTeamPoints == 0 || secondTeamPoints == 0) {
            return calculatePointsWhenValat();
        } else {
            return calculatePointsWhenNoDoubleAndNoValat(firstTeamTotalPoints, secondTeamTotalPoints, bidder, firstTeam);
        }
    }

    private int calculatePointsWhenNoDoubleAndNoValat(int firstTeamTotalPoints, int secondTeamTotalPoints, Player bidder, Team firstTeam) { //TODO add if its not
        if (isBidderFromFirstTeam(bidder, firstTeam) && firstTeamTotalPoints < secondTeamTotalPoints) {
            totalSecondTeamPoints += firstTeamTotalPoints + secondTeamTotalPoints;
            return 0;
        } else if (!isBidderFromFirstTeam(bidder, firstTeam) && secondTeamTotalPoints < firstTeamTotalPoints) {
            totalFirstTeamPoints += firstTeamTotalPoints + secondTeamTotalPoints;
            return 0;
        }
        return addPointsToTeamsIfNotScored(firstTeamTotalPoints, secondTeamTotalPoints, bidder, firstTeam);
    }

    private int addPointsToTeamsIfNotScored(int firstTeamTotalPoints, int secondTeamTotalPoints, Player bidder, Team firstTeam) {
        if (firstTeamTotalPoints == secondTeamTotalPoints) {
            if (isBidderFromFirstTeam(bidder, firstTeam)) {
                totalSecondTeamPoints += secondTeamPoints;
                return firstTeamTotalPoints;
            } else {
                totalFirstTeamPoints += firstTeamPoints;
                return secondTeamTotalPoints;
            }
        }
        totalFirstTeamPoints += firstTeamPoints;
        totalSecondTeamPoints += secondTeamPoints;
        return 0;
    }

    private void addLastTen(Team firstTeam, Team secondTeam, Player moveWinner) {
        if (firstTeam.isPlayerFromThisTeam(moveWinner)) {
            firstTeamPoints += 10;
        } else {
            secondTeamPoints += 10;
        }
    }

    private void addPointsFromPilesAndAnnouncements(List<Card> firstTeamPile, List<Card> secondTeamPile, GameRules currentRule,
                                                    List<Announcement> firstTeamAnnouncements, List<Announcement> secondTeamAnnouncements,
                                                    int firstTeamBelotePoints, int secondTeamBelotePoints) {
        firstTeamPoints += currentRule.calculatePoints(firstTeamPile);
        secondTeamPoints += currentRule.calculatePoints(secondTeamPile);
        firstTeamAnnouncementPoints += AnnouncementPointsCalculator.announcementPointsCalculator(firstTeamAnnouncements);
        secondTeamAnnouncementPoints += AnnouncementPointsCalculator.announcementPointsCalculator(secondTeamAnnouncements);
        firstTeamAnnouncementPoints += firstTeamBelotePoints;
        secondTeamAnnouncementPoints += secondTeamBelotePoints;
    }

    private boolean isBidderFromFirstTeam(Player bidder, Team firstTeam) {
        return firstTeam.isPlayerFromThisTeam(bidder);
    }

    private int addPointsFromLastHanding(int handingPointsFromLastRound) {
        if (handingPointsFromLastRound != 0) {
            if (totalFirstTeamPoints > totalSecondTeamPoints) {
                totalFirstTeamPoints += handingPointsFromLastRound;
                return 0;
            } else if (totalFirstTeamPoints < totalSecondTeamPoints) {
                totalSecondTeamPoints += handingPointsFromLastRound;
                return 0;
            }
        }
        return handingPointsFromLastRound;
    }
}
