package com.vs.belote.game.players;

import com.vs.belote.game.ActionValidator;
import com.vs.belote.game.auxiliaryclasses.Either;
import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Deck;
import com.vs.belote.game.io.InputOutput;
import com.vs.belote.game.rules.GameRules;
import com.vs.belote.game.team.CrawlTeams;

import java.util.*;

public class HumanPlayer implements Player {

    private InputOutput IO;
    private String name;
    private List<Card> hand;

    public HumanPlayer(String name, InputOutput IO) {
        this.name = name;
        this.IO = IO;
        hand = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HumanPlayer that = (HumanPlayer) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Card> getHand() {
        return new ArrayList<>(hand);
    }

    @Override
    public void addCardsToHand(List<Card> dealtCards) {
        hand.addAll(dealtCards);
    }

    @Override
    public Card playCard(List<Card> cardsOnTable, GameRules currentRule, CrawlTeams crawlTeams, Player firstPlayer) {
        int indexForCard = askPlayerForAMove("Pick a card for playing ", 0, hand.size() - 1);
        boolean canIPlayThisCard = currentRule.canIPlayThisCard(cardsOnTable, hand.get(indexForCard), hand, crawlTeams, firstPlayer);
        while (!canIPlayThisCard) {
            indexForCard = askPlayerForAMove("Pick a card for playing ", 0, hand.size() - 1);
            canIPlayThisCard = currentRule.canIPlayThisCard(cardsOnTable, hand.get(indexForCard), hand, crawlTeams, firstPlayer);
        }
        return hand.remove(indexForCard);
    }

    @Override
    public void clearHand(Deck deck) {
        deck.appendCardsToDeck(hand);
        hand.clear();
    }

    @Override
    public int askPlayerForAMove(String message, int minAcceptableNumber, int maxAcceptableNumber) {
        Either<String, Integer> temp = inputVerifier(message, minAcceptableNumber, maxAcceptableNumber);
        while (temp.isLeft()) {
            temp = inputVerifier(message, minAcceptableNumber, maxAcceptableNumber);
        }
        return temp.right();
    }

    @Override
    public Bid makeABid(Bid highestBid, boolean arePlayersFromTheSameTeam) {
        int bidNumber = askPlayerForAMove("Make a bid! Select a number from ", 0, 8);
        while (!ActionValidator.isPossibleBid(highestBid, bidNumber, arePlayersFromTheSameTeam)) {
            bidNumber = askPlayerForAMove("Make a bid! Select a number from ", 0, 8);
        }
        if (bidNumber == 7) {
            highestBid.setDoublePoints(true);
            return highestBid;
        }
        if (bidNumber == 8) {
            highestBid.setReDoublePoints(false);
            return highestBid;
        }
        return Bid.values()[bidNumber];
    }

    private void sortCardsInPlayersHand(Comparator<Card> customCardComparator) {
        Collections.sort(hand, customCardComparator);
    }

    private int askPlayerForMove(String message, int minAcceptableNumber, int maxAcceptableNumber) {
        IO.printLine(message + minAcceptableNumber + "to " + maxAcceptableNumber);
        return Integer.parseInt(IO.readLine());
    }

    private Either<String, Integer> inputVerifier(String message, int minAcceptableNumber, int maxAcceptableNumber) {
        int input = askPlayerForMove(message, minAcceptableNumber, maxAcceptableNumber);
        if (input > maxAcceptableNumber || input < minAcceptableNumber) {
            return Either.left("Wrong input!");
        }
        return Either.right(input);
    }
}
