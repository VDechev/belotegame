package com.vs.belote.game.players;

import com.vs.belote.game.ActionValidator;
import com.vs.belote.game.auxiliaryclasses.Either;
import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Deck;
import com.vs.belote.game.rules.GameRules;
import com.vs.belote.game.team.CrawlTeams;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VirtualPlayer implements Player {

    String name;
    List<Card> hand;

    public VirtualPlayer(String name) {
        this.name = name;
        hand = new ArrayList<Card>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void addCardsToHand(List<Card> dealtCards) {
        hand.addAll(dealtCards);
    }

    @Override
    public Card playCard(List<Card> cardsOnTable, GameRules currentRule, CrawlTeams crawlTeams, Player firstPlayer) {
        int indexForCard = askPlayerForAMove("Pick a card for playing ", 0, hand.size() - 1);
        boolean canIPlayThisCard = currentRule.canIPlayThisCard(cardsOnTable, hand.get(indexForCard), hand, crawlTeams, firstPlayer);
        while (!canIPlayThisCard) {
            indexForCard = askPlayerForAMove("Pick a card for playing ", 0, hand.size() - 1);
            canIPlayThisCard = currentRule.canIPlayThisCard(cardsOnTable, hand.get(indexForCard), hand, crawlTeams, firstPlayer);
        }
        return hand.remove(indexForCard);
    }

    @Override
    public void clearHand(Deck deck) {
        deck.appendCardsToDeck(hand);
        hand.clear();
    }

    @Override
    public int askPlayerForAMove(String message, int minAcceptableNumber, int maxAcceptableNumber) {
        Either<String, Integer> temp = inputVerifier(minAcceptableNumber, maxAcceptableNumber);
        while (temp.isLeft()) {
            temp = inputVerifier(minAcceptableNumber, maxAcceptableNumber);
        }
        return temp.right();
    }

    @Override
    public Bid makeABid(Bid highestBid, boolean arePlayersFromTheSameTeam) {
        int bidNumber = askPlayerForAMove("Make a bid! Select a number from ", 0, 8);
        while (!ActionValidator.isPossibleBid(highestBid, bidNumber, arePlayersFromTheSameTeam)) {
            bidNumber = askPlayerForAMove("Make a bid! Select a number from ", 0, 8);
        }
        if (bidNumber == 7) {
            highestBid.setDoublePoints(true);
            return highestBid;
        }
        if (bidNumber == 8) {
            highestBid.setReDoublePoints(false);
            return highestBid;
        }
        return Bid.values()[bidNumber];
    }

    @Override
    public List<Card> getHand() {
        return new ArrayList<>(hand);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VirtualPlayer that = (VirtualPlayer) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    private Either<String, Integer> inputVerifier(int minAcceptableNumber, int maxAcceptableNumber) {
        int input = pickANumber(maxAcceptableNumber);
        if (input > maxAcceptableNumber || input < minAcceptableNumber) {
            return Either.left("Wrong input!");
        }
        return Either.right(input);
    }

    private int pickANumber(int numberLimit) {
        return (int) (Math.random() * numberLimit + 1);
    }
}
