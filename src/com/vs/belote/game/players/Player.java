package com.vs.belote.game.players;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Deck;
import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.rules.GameRules;
import com.vs.belote.game.team.CrawlTeams;

import java.util.List;

public interface Player {
    String getName();

    List<Card> getHand();

    void addCardsToHand(List<Card> dealtCards);

    Card playCard(List<Card> cardsOnTable, GameRules currentRule, CrawlTeams crawlTeams, Player firstPlayer);

    int askPlayerForAMove(String message, int minAcceptableNumber, int maxAcceptableNumber);

    Bid makeABid(Bid highestBid, boolean arePlayersFromTheSameTeam);

    void clearHand(Deck deck);
}
