package com.vs.belote.game.rules;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.team.CrawlTeams;

import java.util.List;

public interface GameRules {
    Player moveWinner(List<Card> cardsFromMove, CrawlTeams crawlTeams, Player firstPlayerInMove);

    boolean canIPlayThisCard(List<Card> cardsFromThisMove, Card cardForPlaying, List<Card> currentPlayerHand,
                             CrawlTeams crawlTeams, Player firstPlayerInMove);

    int calculatePoints(List<Card> earnedCards);

    int roundPoints(int points);
}
