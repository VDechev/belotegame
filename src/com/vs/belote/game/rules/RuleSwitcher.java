package com.vs.belote.game.rules;

import com.vs.belote.game.bids.Bid;

public class RuleSwitcher {

    public static GameRules getRulesViaBid(Bid currentBid) {
        if (currentBid == Bid.ALLTRUMPS) {
            return new AllTrumpsRules();
        }
        if (currentBid == Bid.NOTRUMPS) {
            return new NoTrumpsRules();
        } else {
            return new RankRules(currentBid);
        }
    }
}
