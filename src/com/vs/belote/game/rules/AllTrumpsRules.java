package com.vs.belote.game.rules;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.team.CrawlTeams;

import java.util.List;

public class AllTrumpsRules implements GameRules {

    public Card getHighestCardFromTrumpMove(List<Card> moveCards) {
        Card currentHighestCard = moveCards.get(0);
        for (int i = 1; i < moveCards.size(); i++) {
            if (currentHighestCard.rank.getIndexOfRank() == moveCards.get(i).rank.getIndexOfRank()
                    && currentHighestCard.suit.getAllTrumpsCardOrderNumber() < moveCards.get(i).suit.getAllTrumpsCardOrderNumber()) {
                currentHighestCard = moveCards.get(i);
            }
        }
        return currentHighestCard;
    }

    @Override
    public Player moveWinner(List<Card> cardsFromMove, CrawlTeams crawlTeams, Player firstPlayerInMove) {
        int winnerIndex = getMoveCardIndexFromCard(getHighestCardFromTrumpMove(cardsFromMove), cardsFromMove);
        Player winner = firstPlayerInMove;

        for (int i = 0; i <= winnerIndex; i++) {
            winner = crawlTeams.getNextPlayer(winner);
        }
        return winner;
    }

    @Override
    public boolean canIPlayThisCard(List<Card> cardsFromThisMove, Card cardForPlaying, List<Card> currentPlayerHand, CrawlTeams crawlTeams,
                                    Player firstPlayerInMove) {
        return isPossibleCardWhenTrump(getHighestCardFromTrumpMove(cardsFromThisMove), cardForPlaying, currentPlayerHand);
    }

    @Override
    public int calculatePoints(List<Card> earnedCards) {
        int points = 0;
        for (Card currentCard : earnedCards) {
            points += currentCard.trumpPoints;
        }
        return points;
    }

    @Override
    public int roundPoints(int points) {
        return (int) ((float) points / 10 + 0.7);
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj || obj instanceof AllTrumpsRules;
    }

    private int getMoveCardIndexFromCard(Card cardForIndex, List<Card> moveCards) {
        for (int i = 0; i < moveCards.size(); i++) {
            if (cardForIndex.equals(moveCards.get(i))) {
                return i;
            }
        }
        return -1;
    }

    private boolean isPossibleCardWhenTrump(Card highestCard, Card playedCard, List<Card> currentPlayerHand) {
        if (isOverrun(highestCard, playedCard)) {
            return true;
        }
        if (!checkIfHandContainsCardToOverrun(highestCard, currentPlayerHand) && playedCard.rank == highestCard.rank) {
            return true;
        }
        return !checkIfCanAnswer(highestCard, currentPlayerHand) && (playedCard.rank != highestCard.rank);
    }

    private boolean isOverrun(Card highestCard, Card playedCard) {
        return highestCard.rank.getIndexOfRank() == playedCard.rank.getIndexOfRank()
                && highestCard.suit.getAllTrumpsCardOrderNumber() < playedCard.suit.getAllTrumpsCardOrderNumber();
    }

    private boolean checkIfHandContainsCardToOverrun(Card highestCard, List<Card> currentPlayerHand) {
        for (Card currentCard : currentPlayerHand) {
            if (highestCard.rank == currentCard.rank && highestCard.suit.getAllTrumpsCardOrderNumber() < currentCard.suit.getAllTrumpsCardOrderNumber()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkIfCanAnswer(Card highestCard, List<Card> currentPlayerHand) {
        for (Card currentCard : currentPlayerHand) {
            if (currentCard.rank == highestCard.rank) {
                return true;
            }
        }
        return false;
    }
}
