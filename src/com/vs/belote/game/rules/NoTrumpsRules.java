package com.vs.belote.game.rules;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.team.CrawlTeams;

import java.util.List;

public class NoTrumpsRules implements GameRules {

    @Override
    public Player moveWinner(List<Card> cardsFromMove, CrawlTeams crawlTeams, Player firstPlayerInMove) {
        int winner = getIndexOfCardWinner(cardsFromMove);
        for (int i = 0; i <= winner; i++) {
            firstPlayerInMove = crawlTeams.getNextPlayer(firstPlayerInMove);
        }
        return firstPlayerInMove;
    }

    @Override
    public boolean canIPlayThisCard(List<Card> cardsFromThisMove, Card cardForPlaying, List<Card> currentPlayerHand, CrawlTeams crawlTeams, Player firstPlayerInMove) {
        if (cardsFromThisMove == null || firstPlayerInMove == null) {
            return true;
        }
        if (cardsFromThisMove.isEmpty() || cardForPlaying.rank == cardsFromThisMove.get(0).rank) {
            return true;
        } else {
            return !checkIfHandContainsMoveFirstPlayedCardRank(cardsFromThisMove.get(0), currentPlayerHand);
        }
    }

    @Override
    public int calculatePoints(List<Card> earnedCards) {
        return earnedCards.stream().mapToInt(p -> p.noTrumpPoints).sum() * 2;
    }

    @Override
    public int roundPoints(int points) {
        return (int) ((float) points / 10 + 0.5);
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj || obj instanceof NoTrumpsRules;
    }

    private boolean checkIfHandContainsMoveFirstPlayedCardRank(Card firstPlayedCard, List<Card> currentPlayerHand) {
        for (Card currentCard : currentPlayerHand) {
            if (firstPlayedCard.rank == currentCard.rank) {
                return true;
            }
        }
        return false;
    }

    private int getIndexOfCardWinner(List<Card> cardsFromMove) {
        Card currentCard = cardsFromMove.get(0);
        int cardIndex = 0;
        for (int i = 1; i < cardsFromMove.size(); i++) {
            if (currentCard.rank.getIndexOfRank() < cardsFromMove.get(i).rank.getIndexOfRank()) {
                currentCard = cardsFromMove.get(i);
                cardIndex = i;
            }
        }
        return cardIndex;
    }
}
