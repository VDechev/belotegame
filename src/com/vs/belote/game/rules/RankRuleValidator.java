package com.vs.belote.game.rules;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.team.CrawlTeams;

import java.util.List;

public class RankRuleValidator {
    public final Bid currentRankBid;

    public RankRuleValidator(Bid currentBid) {
        this.currentRankBid = currentBid;
    }

    public boolean checkIfCardIsCorrect(List<Card> moveCards, Card playedCard, List<Card> currentPlayerHand,
                                        CrawlTeams crawlTeams, Player firstPlayerInMove) {
        if (moveCards.isEmpty()) {
            return true;
        }
        if (moveCards.get(0).rank.getIndexOfRank() == currentRankBid.getIndexOfBid()) {
            return isPossibleCardWhenTrump(getHighestCardFromTrumpMove(moveCards), playedCard, currentPlayerHand);
        } else {
            return isPossibleWhenCardIsNotTrump(moveCards, playedCard, currentPlayerHand, crawlTeams, firstPlayerInMove);
        }
    }

    public Card getHighestCardFromTrumpMove(List<Card> moveCards) {
        Card currentHighestCard = moveCards.get(0);
        for (int i = 1; i < moveCards.size(); i++) {
            if (currentHighestCard.rank.getIndexOfRank() == moveCards.get(i).rank.getIndexOfRank() &&
                    currentHighestCard.suit.getAllTrumpsCardOrderNumber() < moveCards.get(i).suit.getAllTrumpsCardOrderNumber()) {
                currentHighestCard = moveCards.get(i);
            }
        }
        return currentHighestCard;
    }

    public Card getHighestCardIndexWhenNoTrumpMove(List<Card> cardsFromMove) {
        Card currentHighestCard = cardsFromMove.get(0);
        for (int i = 1; i < cardsFromMove.size(); i++) {
            if (currentHighestCard.rank.getIndexOfRank() != currentRankBid.getIndexOfBid() &&
                    isHigherNoTrump(currentHighestCard, cardsFromMove.get(i))) {
                currentHighestCard = cardsFromMove.get(i);
            }
            if (cardsFromMove.get(i).rank.getIndexOfRank() == currentRankBid.getIndexOfBid() &&
                    isHighestTrump(currentHighestCard, cardsFromMove.get(i))) {
                currentHighestCard = cardsFromMove.get(i);
            }
        }
        return currentHighestCard;
    }


    public int getMoveCardIndexFromCard(Card cardForIndex, List<Card> moveCards) {
        for (int i = 0; i < moveCards.size(); i++) {
            if (cardForIndex.equals(moveCards.get(i))) {
                return i;
            }
        }
        return -1;
    }

    private boolean isHighestTrump(Card currentHighestCard, Card currentCard) {
        if (currentHighestCard.rank.getIndexOfRank() == currentRankBid.getIndexOfBid()) {
            if (currentCard.suit.getAllTrumpsCardOrderNumber() > currentHighestCard.suit.getAllTrumpsCardOrderNumber()) {
                return true;
            }
        }
        return true;
    }

    private boolean isHigherNoTrump(Card currentHighestCard, Card currentCard) {
        return currentCard.suit.getNoTrumpsCardOrderNumber() > currentHighestCard.suit.getNoTrumpsCardOrderNumber() &&
                currentCard.rank == currentHighestCard.rank;
    }

    private boolean isPossibleCardWhenTrump(Card highestCard, Card playedCard, List<Card> currentPlayerHand) {
        if (isOverrun(highestCard, playedCard)) {
            return true;
        }
        if (!checkIfHandContainsCardToOverrun(highestCard, currentPlayerHand) && playedCard.rank == highestCard.rank) {
            return true;
        }

        return !checkIfCanAnswer(highestCard, currentPlayerHand) && playedCard.rank != highestCard.rank;
    }

    private boolean isOverrun(Card highestCard, Card playedCard) {
        return highestCard.rank.getIndexOfRank() == playedCard.rank.getIndexOfRank() &&
                highestCard.suit.getAllTrumpsCardOrderNumber() < playedCard.suit.getAllTrumpsCardOrderNumber();
    }

    private boolean checkIfHandContainsCardToOverrun(Card highestCard, List<Card> currentPlayerHand) {
        for (Card currentCard : currentPlayerHand) {
            if (highestCard.rank == currentCard.rank &&
                    highestCard.suit.getAllTrumpsCardOrderNumber() < currentCard.suit.getAllTrumpsCardOrderNumber()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkIfCanAnswer(Card highestCard, List<Card> currentPlayerHand) {
        for (Card currentCard : currentPlayerHand) {
            if (currentCard.rank == highestCard.rank) {
                return true;
            }
        }
        return false;
    }

    private boolean isPossibleWhenCardIsNotTrump(List<Card> cardsFromThisMove, Card cardForPlaying, List<Card> currentPlayerHand,
                                                 CrawlTeams crawlTeams, Player firstPlayerInMove) {
        if (cardForPlaying.rank == cardsFromThisMove.get(0).rank) {
            return true;
        }
        if (checkIfCanAnswer(cardsFromThisMove.get(0), currentPlayerHand)) {
            return false;
        }
        Card currentHighestCard = getHighestCardIndexWhenNoTrumpMove(cardsFromThisMove);
        int highestCardIndex = getMoveCardIndexFromCard(currentHighestCard, cardsFromThisMove);
        if (currentHighestCard.rank.getIndexOfRank() == currentRankBid.getIndexOfBid() &&
                !checkIfTeamWithPlayerWithHighestCard(cardsFromThisMove, crawlTeams, firstPlayerInMove, highestCardIndex)) {
            return isPossibleCardWhenTrump(currentHighestCard, cardForPlaying, currentPlayerHand);
        }
        return true;
    }

    private boolean checkIfTeamWithPlayerWithHighestCard(List<Card> cardsFromMove, CrawlTeams crawlTeams,
                                                         Player firstPlayerInMove, int highestCardIndex) {
        Player playerWithHighestCard = firstPlayerInMove;
        Player currentPlayer = firstPlayerInMove;
        for (int i = 0; i < highestCardIndex; i++) {
            playerWithHighestCard = crawlTeams.getNextPlayer(playerWithHighestCard);
        }
        for (int i = 0; i < cardsFromMove.size(); i++) {
            currentPlayer = crawlTeams.getNextPlayer(currentPlayer);
        }
        return crawlTeams.arePlayersFromSameTeam(playerWithHighestCard, currentPlayer);
    }
}
