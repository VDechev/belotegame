package com.vs.belote.game.rules;

import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.deck.Card;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.team.CrawlTeams;

import java.util.List;

public class RankRules implements GameRules {

    private RankRuleValidator rankRuleValidator;

    public RankRules(Bid currentBid) {
        rankRuleValidator = new RankRuleValidator(currentBid);
    }

    @Override
    public Player moveWinner(List<Card> cardsFromMove, CrawlTeams crawlTeams, Player firstPlayerInMove) {
        int moveCardWinnerIndex;
        if (cardsFromMove.get(0).rank.getIndexOfRank() == rankRuleValidator.currentRankBid.getIndexOfBid()) {
            moveCardWinnerIndex = rankRuleValidator.getMoveCardIndexFromCard(
                    rankRuleValidator.getHighestCardFromTrumpMove(cardsFromMove),
                    cardsFromMove
            );
        } else {
            moveCardWinnerIndex = rankRuleValidator.getMoveCardIndexFromCard(
                    rankRuleValidator.getHighestCardIndexWhenNoTrumpMove(cardsFromMove),
                    cardsFromMove
            );
        }
        return getPlayerFromIndex(firstPlayerInMove, crawlTeams, moveCardWinnerIndex);
    }

    @Override
    public boolean canIPlayThisCard(List<Card> cardsFromThisMove, Card cardForPlaying, List<Card> currentPlayerHand,
                                    CrawlTeams crawlTeams, Player firstPlayerInMove) {
        if (cardsFromThisMove == null || firstPlayerInMove == null) {
            return true;
        }
        return rankRuleValidator.checkIfCardIsCorrect(cardsFromThisMove, cardForPlaying, currentPlayerHand,
                crawlTeams, firstPlayerInMove);
    }

    @Override
    public int calculatePoints(List<Card> earnedCards) {
        int rankRulePoints = 0;
        for (Card currentCard : earnedCards) {
            if (currentCard.rank.getIndexOfRank() == rankRuleValidator.currentRankBid.getIndexOfBid()) {
                rankRulePoints += currentCard.trumpPoints;
            } else {
                rankRulePoints += currentCard.noTrumpPoints;
            }
        }
        return rankRulePoints;
    }

    @Override
    public int roundPoints(int points) {
        return (int) ((float) points / 10 + 0.4);
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof RankRules && this.rankRuleValidator.currentRankBid == ((RankRules) obj).rankRuleValidator.currentRankBid);
    }

    private Player getPlayerFromIndex(Player firstPlayer, CrawlTeams crawlTeams, int indexForCrawling) {
        Player currentPlayer = firstPlayer;
        for (int i = 0; i < indexForCrawling; i++) {
            currentPlayer = crawlTeams.getNextPlayer(currentPlayer);
        }
        return currentPlayer;
    }
}
