package com.vs.belote.game.bids;

import com.vs.belote.game.ActionValidator;
import com.vs.belote.game.auxiliaryclasses.Pair;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.team.CrawlTeams;

public class BidVoter {

    public static Pair<Player, Bid> bid(Player bidder, CrawlTeams crawlTeams) {
        int passCounter = 0;
        Player highestBidder = null;
        Bid highestBid = null;
        Bid currentBid;
        do {
            boolean arePlayersFromTheSameTeam = ActionValidator.arePlayersFromTheSameTeam(bidder, highestBidder, crawlTeams);
            currentBid = bidder.makeABid(highestBid, arePlayersFromTheSameTeam);
            if (currentBid == Bid.PASS) {
                passCounter++;

            } else {
                highestBid = currentBid;
                highestBidder = bidder;
                passCounter = 0;
            }
            bidder = crawlTeams.getNextPlayer(bidder);
        } while (!isBiddingDone(passCounter, highestBid));

        return new Pair<>(highestBidder, highestBid);
    }

    private static boolean isBiddingDone(int passCounter, Bid highestBid) {
        return (highestBid != null && passCounter == 3) || passCounter == 4;
    }
}
