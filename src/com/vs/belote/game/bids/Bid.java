package com.vs.belote.game.bids;

public enum Bid {
    PASS(0), SPADES(1), DIAMONDS(2), HEARTHS(3), CLUBS(4), NOTRUMPS(5), ALLTRUMPS(6);

    private boolean doublePoints = false;
    private boolean reDoublePoints = false;
    private int indexOfBid;

    Bid(int indexOfBid) {
        this.indexOfBid = indexOfBid;
    }

    public int getIndexOfBid() {
        return indexOfBid;
    }

    public boolean isDoublePoints() {
        return doublePoints;
    }

    public void setDoublePoints(boolean doublePoints) {
        this.doublePoints = doublePoints;
    }

    public boolean isReDoublePoints() {
        return reDoublePoints;
    }

    public void setReDoublePoints(boolean reDoublePoints) {
        this.reDoublePoints = reDoublePoints;
    }


}
