package com.vs.belote.game.io;

import java.util.Scanner;

public class CommandLineIO implements InputOutput {

    private Scanner scanner;

    public CommandLineIO() {
        this.scanner = new Scanner(System.in);
    }

    @Override
    public String readLine() {
        return scanner.nextLine();
    }

    @Override
    public void printLine(String output) {
        System.out.println(output);
    }
}
