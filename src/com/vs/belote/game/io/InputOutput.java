package com.vs.belote.game.io;

public interface InputOutput {

    String readLine();

    void printLine(String output);
}
