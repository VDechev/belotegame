package com.vs.belote.game;

import com.vs.belote.game.deck.Deck;
import com.vs.belote.game.io.CommandLineIO;
import com.vs.belote.game.io.InputOutput;
import com.vs.belote.game.team.Team;

import java.io.File;

public class Game {

    public void playGame(Room room, BeloteTable beloteTable, InputOutput IO) {
        Team winnerTeam = room.playBeloteGame(beloteTable);
        IO.printLine(winnerMessage(winnerTeam));
    }

    private String winnerMessage(Team teamWinner) {
        return "The winners are " + teamWinner.getFirstPlayer().getName() + " and " +
                teamWinner.getSecondPlayer().getName() + " with " + teamWinner.getTeamPoints() + " points";
    }

    public static void main(String[] args) {
        File beloteCardsFile = new File("Cards.txt");
        InputOutput IO = new CommandLineIO();
        Deck deck = new Deck(beloteCardsFile, IO);
        Room room = new Room();
        BeloteTable beloteTable = new BeloteTable(deck);

        Game game = new Game();
        game.playGame(room, beloteTable, IO);
    }
}
