package com.vs.belote.game;

import com.vs.belote.game.announcements.Announcement;
import com.vs.belote.game.announcements.AnnouncementChecker;
import com.vs.belote.game.announcements.AnnouncementComparator;
import com.vs.belote.game.announcements.BeloteAnnouncement;
import com.vs.belote.game.auxiliaryclasses.Either;
import com.vs.belote.game.auxiliaryclasses.Pair;
import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.bids.BidVoter;
import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Deck;
import com.vs.belote.game.deck.Suit;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.rules.GameRules;
import com.vs.belote.game.rules.RuleSwitcher;
import com.vs.belote.game.team.CrawlTeams;
import com.vs.belote.game.team.Team;

import java.util.ArrayList;
import java.util.List;

public class Round {
    private int firstTeamBelotePoints = 0;
    private int secondTeamBelotePoints = 0;
    PointsCalculator pointsCalculator = new PointsCalculator();

    public int round(Team firstTeam, Team secondTeam, CrawlTeams crawlTeams, Deck deck, Player dealer, int handingPointsFromLastRound) {
        List<Card> firstTeamPile = new ArrayList<>();
        List<Card> secondTeamPile = new ArrayList<>();
        List<Announcement> firstTeamAnnouncements = new ArrayList<>();
        List<Announcement> secondTeamAnnouncements = new ArrayList<>();

        Pair<Player, Bid> currentBid = makeABidAndGetPair(crawlTeams, deck, dealer);
        GameRules currentRule = RuleSwitcher.getRulesViaBid(currentBid.getValue());

        Player firstPlayer = crawlTeams.getNextPlayer(dealer); // its Bidder now
        Player winnerPlayer = playMovesAndGetLastWinner(currentRule, firstTeamPile, secondTeamPile, crawlTeams, firstPlayer, firstTeam, firstTeamAnnouncements,
                secondTeamAnnouncements, currentBid.getValue());

        compareAnnotations(firstTeamAnnouncements, secondTeamAnnouncements);
        cleanUpAfterRound(deck, dealer, crawlTeams);

        return pointsCalculator.calculateTeamsPoints(firstTeam, secondTeam, firstTeamPile, secondTeamPile, currentRule, winnerPlayer,
                currentBid.getKey(), currentBid.getValue().isDoublePoints(), currentBid.getValue().isReDoublePoints(), handingPointsFromLastRound,
                firstTeamAnnouncements, secondTeamAnnouncements, firstTeamBelotePoints, secondTeamBelotePoints);
    }

    private Player playMovesAndGetLastWinner(GameRules currentRule, List<Card> firstTeamPile, List<Card> secondTeamPile, CrawlTeams crawlTeams, Player firstPlayer, Team firstTeam,
                                             List<Announcement> firstTeamAnnouncements, List<Announcement> secondTeamAnnouncements, Bid currentBid) {
        for (int i = 0; i < 8; i++) {
            Player currentPlayer = firstPlayer;
            List<Card> playedCards = new ArrayList<>();
            playedCards.add(currentPlayer.playCard(playedCards, currentRule, crawlTeams, firstPlayer));
            playMove(currentPlayer, crawlTeams, playedCards, currentRule, firstPlayer, i, currentBid, firstTeam, firstTeamAnnouncements, secondTeamAnnouncements);

            firstPlayer = currentRule.moveWinner(playedCards, crawlTeams, firstPlayer);
            if (firstTeam.isPlayerFromThisTeam(firstPlayer)) {
                firstTeamPile.addAll(playedCards);
            } else {
                secondTeamPile.addAll(playedCards);
            }
        }
        return firstPlayer;
    }

    private Pair<Player, Bid> makeABidAndGetPair(CrawlTeams crawlTeams, Deck deck, Player dealer) {
        deck.dealToAllPlayers(crawlTeams, dealer);
        deck.dealToAllPlayers(crawlTeams, dealer);
        Pair<Player, Bid> roundBid = BidVoter.bid(crawlTeams.getNextPlayer(dealer), crawlTeams);
        while (roundBid.getValue() == null) {
            clearPlayersHand(dealer, crawlTeams, deck);
            deck.splitDeck(dealer.askPlayerForAMove("It's your time to split the deck! Choose a index for splitting from ", 2, 31));
            dealer = crawlTeams.getNextPlayerToDeal();
            deck.dealToAllPlayers(crawlTeams, dealer);
            deck.dealToAllPlayers(crawlTeams, dealer);
            roundBid = BidVoter.bid(crawlTeams.getNextPlayer(dealer), crawlTeams);
        }
        return roundBid;
    }

    private void clearPlayersHand(Player player, CrawlTeams crawlTeams, Deck deck) {
        player.clearHand(deck);
        for (int i = 0; i < 3; i++) {
            player = crawlTeams.getNextPlayer(player);
            player.clearHand(deck);
        }
    }

    private void addAnnouncements(Player currentPlayer, List<Announcement> firstTeamAnnouncements, Team firstTeam, List<Announcement> secondTeamAnnouncements,
                                  int moveIndex) {
        if (moveIndex == 0) {
            if (firstTeam.isPlayerFromThisTeam(currentPlayer)) {
                firstTeamAnnouncements.addAll(AnnouncementChecker.getPlayerAnnouncements(currentPlayer.getHand()));
            } else {
                secondTeamAnnouncements.addAll(AnnouncementChecker.getPlayerAnnouncements(currentPlayer.getHand()));
            }
        }
    }

    private void addBelote(Player currentPlayer, Team firstTeam) {
        if (firstTeam.isPlayerFromThisTeam(currentPlayer)) {
            firstTeamBelotePoints += 20;
        } else {
            secondTeamBelotePoints += 20;
        }
    }

    private void compareAnnotations(List<Announcement> firstTeamAnnouncements, List<Announcement> secondTeamAnnouncements) {
        Either announcements = AnnouncementComparator.announceComparator(firstTeamAnnouncements, secondTeamAnnouncements);
        if (announcements == null) {
            firstTeamAnnouncements.clear();
            secondTeamAnnouncements.clear();
        } else if (announcements.isLeft()) {
            secondTeamAnnouncements.clear();

        } else if (announcements.isRight()) {
            firstTeamAnnouncements.clear();
        }
    }

    private void addBelote(Card playedCard, Player currentPlayer, Bid currentBid, Team firstTeam) {
        if ((playedCard.suit == Suit.KING || playedCard.suit == Suit.QUEEN) && BeloteAnnouncement.isBelote(playedCard, currentPlayer.getHand(), currentBid)) {
            addBelote(currentPlayer, firstTeam);
        }
    }

    private void playMove(Player currentPlayer, CrawlTeams crawlTeams, List<Card> playedCards, GameRules currentRule, Player firstPlayer, int moveIndex,
                          Bid currentBid, Team firstTeam, List<Announcement> firstTeamAnnouncements, List<Announcement> secondTeamAnnouncements) {
        for (int j = 0; j < 3; j++) {
            currentPlayer = crawlTeams.getNextPlayer(currentPlayer);
            addAnnouncements(currentPlayer, firstTeamAnnouncements, firstTeam, secondTeamAnnouncements, moveIndex);
            Card playedCard = currentPlayer.playCard(playedCards, currentRule, crawlTeams, firstPlayer);
            addBelote(playedCard, currentPlayer, currentBid, firstTeam);
            playedCards.add(playedCard);

        }
    }

    public void cleanUpAfterRound(Deck deck, Player player, CrawlTeams crawlTeams) {
        for (Bid bid : Bid.values()) {
            bid.setDoublePoints(false);
            bid.setReDoublePoints(false);
        }
        for (int i = 0; i < 4; i++) {
            player = crawlTeams.getNextPlayer(player);
            player.clearHand(deck);
        }
    }
}
