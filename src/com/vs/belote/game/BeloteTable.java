package com.vs.belote.game;

import com.vs.belote.game.auxiliaryclasses.Pair;
import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.deck.Deck;
import com.vs.belote.game.io.CommandLineIO;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.team.CrawlTeams;
import com.vs.belote.game.team.FillTeams;
import com.vs.belote.game.team.Team;

public class BeloteTable implements Table {

    private Team firstTeam;
    private Team secondTeam;
    private CrawlTeams crawlTeams;
    private Deck deck;
    private Pair<Player, Bid> roundBid;
    private Player dealer;

    public BeloteTable(Deck deck) {
        this.deck = deck;
        FillTeams fillTeams = new FillTeams(new CommandLineIO(), deck.getTwoBlackAndTwoRedCards());
        firstTeam = fillTeams.getTeamWithBlackCards();
        secondTeam = fillTeams.getTeamWithRedCards();
        crawlTeams = new CrawlTeams(firstTeam, secondTeam, chooseDealer());
    }

    @Override
    public Team playGameAndGetWinner() {
        int handingPoints = 0;
        while ((firstTeam.getTeamPoints() < 150 || secondTeam.getTeamPoints() < 150) && (firstTeam.getTeamPoints() != secondTeam.getTeamPoints())) {
            Round currentRound = new Round();
            handingPoints = currentRound.round(firstTeam, secondTeam, crawlTeams, deck, dealer, handingPoints);
        }
        return getTeamWinner();
    }

    private int chooseDealer() {
        return (int) (Math.random() * 4);
    }

    private Team getTeamWinner() {
        if (firstTeam.getTeamPoints() > secondTeam.getTeamPoints()) {
            return firstTeam;
        }
        return secondTeam;
    }
}
