package com.vs.belote.game;

import com.vs.belote.game.bids.Bid;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.team.CrawlTeams;

public class ActionValidator {

    private static final int DOUBLE = 7;
    private static final int RE_DOUBLE = 8;

    public static boolean isPossibleBid(Bid currentHighestBid, int newBidNumber, boolean arePlayersFromTheSameTeam) {
        if ((newBidNumber == DOUBLE || newBidNumber == RE_DOUBLE) && !arePlayersFromTheSameTeam) {
            return checkIfPossibleDoubleOrReDouble(newBidNumber, currentHighestBid);

        } else if (newBidNumber < DOUBLE) {
            return isValidBid(currentHighestBid, Bid.values()[newBidNumber]);
        }
        return false;
    }

    public static boolean arePlayersFromTheSameTeam(Player playerWithHighestBid, Player currentBidder, CrawlTeams crawlTeams) {
        return (crawlTeams.arePlayersFromSameTeam(playerWithHighestBid, currentBidder));
    }

    private static boolean isValidBid(Bid currentHighestBid, Bid newBid) {
        return (currentHighestBid.getIndexOfBid() < newBid.getIndexOfBid() || newBid == Bid.PASS);
    }

    private static boolean checkIfPossibleDoubleOrReDouble(int newBidNumber, Bid currentHighestBid) {
        return currentHighestBid != null && currentHighestBid != Bid.PASS
                && ((newBidNumber == DOUBLE && !currentHighestBid.isDoublePoints())
                || (newBidNumber == RE_DOUBLE && currentHighestBid.isDoublePoints() && !currentHighestBid.isReDoublePoints()))
                ;
    }


}
