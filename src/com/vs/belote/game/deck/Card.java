package com.vs.belote.game.deck;

import java.util.Objects;

public class Card implements Comparable<Card> {

    public final Rank rank;
    public final Suit suit;
    public final int trumpPoints;
    public final int noTrumpPoints;

    public Card(Rank rank, Suit suit, int trumpPoints, int noTrumpPoints) {
        this.rank = rank;
        this.suit = suit;
        this.trumpPoints = trumpPoints;
        this.noTrumpPoints = noTrumpPoints;
    }

    public boolean isRed() {
        return rank == Rank.DIAMONDS || rank == Rank.HEARTS;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Card)) {
            return false;
        }

        return this.rank == ((Card) o).rank && this.suit == ((Card) o).suit && this.noTrumpPoints == ((Card) o).noTrumpPoints &&
                this.trumpPoints == ((Card) o).trumpPoints;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rank, suit, trumpPoints, noTrumpPoints);
    }

    @Override
    public int compareTo(Card otherCard) {
        if (this.rank != otherCard.rank) {
            return this.rank.compareTo(otherCard.rank);
        }
        return 0;

    }
}

