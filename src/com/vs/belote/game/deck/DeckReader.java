package com.vs.belote.game.deck;

import com.vs.belote.game.auxiliaryclasses.Either;
import com.vs.belote.game.io.InputOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public final class DeckReader {

    final int BELOTE_CARDS_ALLTRUMPS_POINTS = 248;
    final int BELOTE_CARDS_NOTRUMPS_POINTS = 120;
    final int BELOTE_CARDS_LENGTH = 32;

    public final List<Card> createDeck(File beloteCardsFile, InputOutput IO) {
        Either<String, List<Card>> deck;
        do {
            deck = openAndFillDeck(beloteCardsFile);
            if (deck.isLeft()) {
                IO.printLine(deck.left());
                System.out.println(deck.left());
                beloteCardsFile = inputNewFile(IO);
            }
        }
        while (deck.isLeft());

        return deck.right();
    }

    private File inputNewFile(InputOutput IO) {
        IO.printLine("Enter new file path");
        return new File(IO.readLine());
    }

    private Either<String, List<Card>> openAndFillDeck(File beloteCardsFile) //throws FileNotFoundException,IOException
    {
        List<Card> deck = new ArrayList<>();
        Scanner sc = null;
        try {
            sc = new Scanner(beloteCardsFile);
        } catch (FileNotFoundException e) {
            return Either.left("Can't open the current file");
        }
        while (sc.hasNextLine()) {
            String readLine = sc.nextLine();
            String[] separateCard = readLine.split(" ");
            deck.add(new Card(Rank.valueOf(separateCard[0]), Suit.valueOf(separateCard[1]),
                    Integer.parseInt(separateCard[2]), Integer.parseInt(separateCard[3])));
        }
        if (!cardsVerifier(deck)) {
            return Either.left("Invalid cards information");
        }
        return Either.right(deck);
    }

    private boolean verifyCardsNumber(List<Card> cardsToVerify) {
        return cardsToVerify.size() == BELOTE_CARDS_LENGTH;
    }

    private boolean verifyCardsAllTrumpsPoints(List<Card> cardsToVerify) {
        int points = 0;

        for (Card c :
                cardsToVerify) {
            points += c.trumpPoints;
        }
        return BELOTE_CARDS_ALLTRUMPS_POINTS == points;
    }

    private boolean verifyCardsNoTrumpPoints(List<Card> cardsToVerify) {
        int points = 0;

        for (Card c :
                cardsToVerify) {
            points += c.noTrumpPoints;
        }
        return BELOTE_CARDS_NOTRUMPS_POINTS == points;
    }

    private boolean cardsVerifier(List<Card> cardsToVerify) {
        return (verifyCardsNumber(cardsToVerify)
                && verifyCardsAllTrumpsPoints(cardsToVerify)
                && verifyCardsNoTrumpPoints(cardsToVerify));
    }
}
