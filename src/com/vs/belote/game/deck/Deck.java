package com.vs.belote.game.deck;

import com.vs.belote.game.io.InputOutput;
import com.vs.belote.game.players.Player;
import com.vs.belote.game.team.CrawlTeams;

import java.io.File;
import java.util.*;

public class Deck {

    public final Deque<Card> deck;

    public Deck(File beloteCardsFile, InputOutput IO) {
        DeckReader readDeck = new DeckReader();
        List<Card> deckToShuffle = new ArrayList<>(readDeck.createDeck(beloteCardsFile, IO));
        Collections.shuffle(deckToShuffle);
        deck = new ArrayDeque<>(deckToShuffle);
    }

    public void appendCardsToDeck(List<Card> cardsForAppend) {
        deck.addAll(cardsForAppend);
    }

    public List<Card> getTwoBlackAndTwoRedCards() {
        List<Card> listWithFourCards = new ArrayList<>();
        int redCounter = 0;
        int blackCounter = 0;
        for (Card card : deck) {
            if (card.isRed() && redCounter < 2) {
                listWithFourCards.add(card);
                redCounter++;
            } else if (!card.isRed() && blackCounter < 2) {
                listWithFourCards.add(card);
                blackCounter++;
            }
            if (redCounter == 2 && blackCounter == 2) {
                break;
            }
        }
        return listWithFourCards;
    }

    public void splitDeck(int splitIndex) {

        Collections.rotate((List<?>) deck, splitIndex);
    }

    public void dealToAllPlayers(CrawlTeams crawlTeams, Player dealer) {
        Player player = crawlTeams.getNextPlayer(dealer);
        for (int i = 0; i < 4; i++) {
            player.addCardsToHand(deal(player));
            player = crawlTeams.getNextPlayer(player);
        }
    }

    private List<Card> deal(Player player) {
        int numberOfCardsForDealing = 3;
        if (deck.size() <= 20 && deck.size() > 12) {
            numberOfCardsForDealing = 2;
        }
        List<Card> cardsToAdd = new ArrayList<>();
        for (int i = 0; i < numberOfCardsForDealing; i++) {
            cardsToAdd.add(deck.removeLast());
        }
        return cardsToAdd;
    }

}
