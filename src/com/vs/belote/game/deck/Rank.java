package com.vs.belote.game.deck;

public enum Rank {
    SPADES(1), DIAMONDS(2), HEARTS(3), CLUBS(4);

    private int indexOfRank;

    Rank(int indexOfRank) {
        this.indexOfRank = indexOfRank;
    }

    public int getIndexOfRank() {
        return indexOfRank;
    }
}