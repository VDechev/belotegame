package com.vs.belote.game.deck;

public enum Suit {
    SEVEN(7, 1, 1),
    EIGHT(8, 2, 2),
    NINE(9, 3, 7),
    TEN(10, 7, 5),
    JACK(11, 4, 8),
    QUEEN(12, 4, 3),
    KING(13, 6, 4),
    ACE(14, 8, 6);

    private int indexOfSuit;
    private int noTrumpsCardOrderNumber;
    private int allTrumpsCardOrderNumber;

    Suit(int indexOfSuit, int noTrumpsCardOrderNumber, int allTrumpsCardOrderNumber) {
        this.indexOfSuit = indexOfSuit;
        this.noTrumpsCardOrderNumber = noTrumpsCardOrderNumber;
        this.allTrumpsCardOrderNumber = allTrumpsCardOrderNumber;
    }

    public int getIndexOfSuit() {
        return indexOfSuit;
    }

    public int getNoTrumpsCardOrderNumber() {
        return noTrumpsCardOrderNumber;
    }

    public int getAllTrumpsCardOrderNumber() {
        return allTrumpsCardOrderNumber;
    }
}
