package com.vs.belote.game.announcements;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Rank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AnnouncementChecker {

    public static List<Announcement> getPlayerAnnouncements(List<Card> playersHand) {
        List<Announcement> playersAnnouncement = new ArrayList<>();
        List<Card> handForChecking = new ArrayList<>(playersHand);

        recursivelyCheckForSquare(handForChecking, playersAnnouncement);
        checkForQuinte(handForChecking, playersAnnouncement);
        recursivelyCheckForQuarte(handForChecking, playersAnnouncement);
        recursivelyCheckForTierce(handForChecking, playersAnnouncement);

        if (playersAnnouncement.size() != 0) {
            return playersAnnouncement;
        }
        return Collections.emptyList();
    }

    private static Announcement checkForSquare(List<Card> playersHand) {
        if (playersHand.size() < 4) {
            return null;
        }
        for (int i = 9; i <= 14; i++) {
            Announcement currentAnnouncement = checkForSquareForOneSuit(playersHand, i);
            if (currentAnnouncement != null) {
                return currentAnnouncement;
            }
        }
        return null;
    }

    private static Announcement checkForSquareForOneSuit(List<Card> playersHand, int suitIndex) {
        List<Card> announcement = playersHand.stream().
                filter(s -> s.suit.getIndexOfSuit() == suitIndex).collect(Collectors.toList());
        if (announcement.size() == 4) {
            int announcementPoints = 100;
            if (suitIndex == 11) {
                announcementPoints = 200;
            }
            if (suitIndex == 9) {
                announcementPoints = 150;
            }
            Card highestCard = announcement.get(0);
            playersHand.removeAll(announcement);
            return new Announcement(Announcements.SQUARE, announcementPoints, highestCard);
        }
        return null;
    }

    private static Announcement checkForQuinte(List<Card> playersHand) {
        if (playersHand.size() < 5) {
            return null;
        }
        for (Rank rank : Rank.values()) {
            Card highestCardInQuinte = checkForCombination(rank, playersHand, 5);
            if (highestCardInQuinte != null) {
                return new Announcement(Announcements.QUINTE, 100, highestCardInQuinte);
            }
        }
        return null;
    }

    private static Announcement checkForQuarte(List<Card> playersHand) {
        if (playersHand.size() < 4) {
            return null;
        }
        for (Rank rank : Rank.values()) {
            Card highestCardInQuarte = checkForCombination(rank, playersHand, 4);
            if (highestCardInQuarte != null) {
                return new Announcement(Announcements.QUARTE, 50, highestCardInQuarte);
            }
        }
        return null;
    }

    public static Announcement checkForTierce(List<Card> playersHand) {
        if (playersHand.size() < 3) {
            return null;
        }
        for (Rank rank : Rank.values()) {
            Card highestCardInTierce = checkForCombination(rank, playersHand, 3);
            if (highestCardInTierce != null) {
                return new Announcement(Announcements.TIERCE, 20, highestCardInTierce);
            }
        }
        return null;
    }

    private static Card checkForCombination(Rank rank, List<Card> playersHand, int size) {
        int counter = 1;
        List<Card> filteredCard = filterCards(playersHand, rank);
        List<Card> usedForCombination = new ArrayList<>();
        if (!filteredCard.isEmpty()) {
            Card current = filteredCard.get(0);
            for (int i = 1; i < filteredCard.size(); i++) {
                if (current.suit.getIndexOfSuit() + 1 == filteredCard.get(i).suit.getIndexOfSuit()) {
                    counter++;
                    usedForCombination.add(filteredCard.get(i - 1));
                } else {
                    counter = 1;
                    usedForCombination = new ArrayList<>();
                }
                current = filteredCard.get(i);
                if (counter == size) {
                    usedForCombination.add(filteredCard.get(i));
                    playersHand.removeAll(usedForCombination);
                    return current;
                }
            }
        }
        return null;
    }

    private static boolean recursivelyCheckForQuarte(List<Card> playersHand, List<Announcement> playersAnnouncement) {
        Announcement announcement = checkForQuarte(playersHand);
        if (announcement != null) {
            playersAnnouncement.add(announcement);
            return recursivelyCheckForQuarte(playersHand, playersAnnouncement);
        } else {
            return false;
        }
    }

    private static boolean recursivelyCheckForSquare(List<Card> playersHand, List<Announcement> playersAnnouncement) {
        Announcement announcement = checkForSquare(playersHand);
        if (announcement != null) {
            playersAnnouncement.add(announcement);
            return recursivelyCheckForSquare(playersHand, playersAnnouncement);
        } else {
            return false;
        }
    }

    private static boolean recursivelyCheckForTierce(List<Card> playersHand, List<Announcement> playersAnnouncement) {
        Announcement announcement = checkForTierce(playersHand);
        if (announcement != null) {
            playersAnnouncement.add(announcement);
            return recursivelyCheckForTierce(playersHand, playersAnnouncement);
        } else {
            return false;
        }
    }

    private static boolean checkForQuinte(List<Card> playersHand, List<Announcement> playersAnnouncement) {
        Announcement announcement = checkForQuinte(playersHand);
        if (announcement != null) {
            playersAnnouncement.add(announcement);
            return true;
        }
        return false;
    }

    private static List<Card> filterCards(List<Card> playersHand, Rank rank) {
        List<Card> filteredCard = playersHand.stream().filter(card -> card.rank == rank)
                .sorted(Comparator.comparingInt(o -> o.suit.getIndexOfSuit()))
                .collect(Collectors.toList());
        return filteredCard;
    }
}
