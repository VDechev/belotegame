package com.vs.belote.game.announcements;

import java.util.List;

public class AnnouncementPointsCalculator {

    public static int announcementPointsCalculator(List<Announcement> teamAnnouncements) {
        int points = 0;
        for (Announcement announcement :
                teamAnnouncements) {
            points += announcement.announcementPoints;
        }
        return points;
    }
}
