package com.vs.belote.game.announcements;

import com.vs.belote.game.auxiliaryclasses.Either;

import java.util.Collections;
import java.util.List;

public class AnnouncementComparator {

    public static Either<List<Announcement>, List<Announcement>> announceComparator(List<Announcement> firstTeamAnnouncements, List<Announcement> secondTeamAnnouncements) {

        Collections.sort(firstTeamAnnouncements);
        Collections.sort(secondTeamAnnouncements);
        if (firstTeamAnnouncements.isEmpty() || secondTeamAnnouncements.isEmpty()) {
            return compareIsEmpty(firstTeamAnnouncements, secondTeamAnnouncements);
        } else {
            return compareIfNotEmpty(firstTeamAnnouncements, secondTeamAnnouncements);
        }
    }

    private static Either<List<Announcement>, List<Announcement>> compareIsEmpty(List<Announcement> firstTeamAnnouncements, List<Announcement> secondTeamAnnouncements) {
        if (secondTeamAnnouncements.isEmpty()) {
            return Either.left(firstTeamAnnouncements);
        }

        if (firstTeamAnnouncements.isEmpty()) {
            return Either.right(secondTeamAnnouncements);
        }
        return null;
    }

    private static Either<List<Announcement>, List<Announcement>> compareIfNotEmpty(List<Announcement> firstTeamAnnouncements, List<Announcement> secondTeamAnnouncements) {
        if (firstTeamAnnouncements.get(0).compareTo(secondTeamAnnouncements.get(0)) > 0) {
            return Either.left(firstTeamAnnouncements);
        }
        if (firstTeamAnnouncements.get(0).compareTo(secondTeamAnnouncements.get(0)) < 0) {
            return Either.right(secondTeamAnnouncements);
        }
        return null;
    }
}
