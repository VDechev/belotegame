package com.vs.belote.game.announcements;

public enum Announcements {
    TIERCE(0), QUARTE(1), QUINTE(2), SQUARE(3);

    Announcements(int announcementPriority) {
        this.announcementPriority = announcementPriority;
    }

    private int announcementPriority;

    public int getAnnouncementPriority() {
        return announcementPriority;
    }
}
