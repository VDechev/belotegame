package com.vs.belote.game.announcements;

import com.vs.belote.game.deck.Card;

public class Announcement implements Comparable {

    public final Announcements announcement;
    public final int announcementPoints;
    public final Card highestAnnouncementCard;

    public Announcement(Announcements announcement, int announcementPoints, Card highestAnnouncementCard) {
        this.announcement = announcement;
        this.highestAnnouncementCard = highestAnnouncementCard;
        this.announcementPoints = announcementPoints;
    }

    @Override
    public int compareTo(Object o) {
        Announcement objectForCompare = (Announcement) o;
        if (objectForCompare == null) {
            return 1;
        }
        if (this.announcement == objectForCompare.announcement) {
            if (this.announcement == Announcements.SQUARE) {
                return this.highestAnnouncementCard.suit.getAllTrumpsCardOrderNumber() - objectForCompare.highestAnnouncementCard.suit.getAllTrumpsCardOrderNumber();
            }
            return this.highestAnnouncementCard.suit.getNoTrumpsCardOrderNumber() - objectForCompare.highestAnnouncementCard.suit.getNoTrumpsCardOrderNumber();
        }
        return this.announcement.getAnnouncementPriority() - objectForCompare.announcement.getAnnouncementPriority();
    }

    @Override
    public boolean equals(Object obj) {
        Announcement objectForCompare = (Announcement) obj;
        if (objectForCompare == null) {
            return false;
        }

        if ((objectForCompare.announcement == this.announcement) && (this.announcementPoints == objectForCompare.announcementPoints)
                && (this.highestAnnouncementCard.equals(objectForCompare.highestAnnouncementCard))) {
            return true;
        }

        return false;
    }
}
