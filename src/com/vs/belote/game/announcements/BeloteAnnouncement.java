package com.vs.belote.game.announcements;

import com.vs.belote.game.deck.Card;
import com.vs.belote.game.deck.Suit;
import com.vs.belote.game.bids.Bid;

import java.util.List;

public class BeloteAnnouncement {
    public static final boolean isBelote(Card cardForBelote, List<Card> currentPlayerHand, Bid currentBid) {
        if (cardForBelote.suit.getIndexOfSuit() > 13 || cardForBelote.suit.getIndexOfSuit() < 12) {
            return false;
        }
        if (cardForBelote.rank.getIndexOfRank() == currentBid.getIndexOfBid() || currentBid == Bid.ALLTRUMPS) {
            return isHandContainingOtherCardForBelote(cardForBelote, currentPlayerHand);
        }
        return false;
    }

    private static boolean isHandContainingOtherCardForBelote(Card cardForBelote, List<Card> currentPlayerHand) {
        for (Card currentCard : currentPlayerHand) {
            if (currentCard.rank == cardForBelote.rank &&
                    (cardForBelote.suit.getIndexOfSuit() + getOtherCardSuitIndexForBelote(cardForBelote)) == (currentCard.suit.getIndexOfSuit())) {
                return true;
            }
        }
        return false;
    }

    private static int getOtherCardSuitIndexForBelote(Card cardForBelote) {
        if (cardForBelote.suit == Suit.QUEEN) {
            return 1;
        } else {
            return -1;
        }
    }
}
