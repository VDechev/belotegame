package com.vs.belote.game;

import com.vs.belote.game.team.Team;

public interface Table {
    Team playGameAndGetWinner();
}
